using BlogApp.Api.Filters;
using BlogApp.Api.Infrastructure.Config;
using BlogApp.Api.Infrastructure.Extensions.StartupConfigurations;
using BlogApp.Api.Infrastructure.Context;
using FluentValidation.AspNetCore;
using IdentityModel.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace BlogApp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = "Bearer";
                    options.DefaultChallengeScheme = "Bearer";
                })
                .AddIdentityServerAuthentication("Bearer",
                    jwtOptions =>
                    {
                        jwtOptions.Authority = Configuration.GetValue<string>("IdentityUrl");
                        jwtOptions.RequireHttpsMetadata = false;
                        jwtOptions.Audience = "BlogApp.Api";
                    },
                    referenceOptions =>
                    {
                        referenceOptions.Authority = Configuration.GetValue<string>("IdentityUrl");
                        referenceOptions.DiscoveryPolicy = new DiscoveryPolicy
                        {
                            Authority = Configuration.GetValue<string>("IdentityUrl"),
                            ValidateIssuerName = false,
                            RequireHttps = false
                        };
                        referenceOptions.ClientId = "BlogApp.Api";
                        referenceOptions.ClientSecret = "e579879b-ad1f-4e6f-a73e-0eb452b6819c";
                    }
                );

            services.AddDbContext<BlogAppDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddOptions();

            services.AddControllers(config =>
            {
                config.Filters.Add(typeof(RequestValidationFilter));
            })
            .AddFluentValidation(config => config.RegisterValidatorsFromAssemblyContaining<Startup>())
            .AddNewtonsoftJson(options =>
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddHttpContextAccessor();

            services.RegisterApplicationDependencyInjection();

            services.AddRouting(options => options.LowercaseUrls = true);

            services.Configure<GrpcApiConfig>(Configuration.GetSection("Grpc"));

            services.AddGrpcClient(Configuration.GetSection("Grpc"));

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAllPolicy");
            app.UseRouting();
            
            app.UseStaticFiles();
            app.UseCustomExceptionHandler(env.IsProduction());
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
