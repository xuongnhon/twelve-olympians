﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Handlers;

namespace BlogApp.Api.Commands
{
    public interface IMenuCommandHandler :
        IBaseCommandHandler,
        ICommandHandler<AddMenuCommandRequestDto, AddMenuCommandResponseDto>,
        ICommandHandler<EditMenuCommandRequestDto, EditMenuCommandResponseDto>,
        ICommandHandler<DeleteMenuCommandRequestDto, DeleteMenuCommandResponseDto>
    {

    }
}
