﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Handlers;

namespace BlogApp.Api.Commands
{
    public interface ICarouselCommandHandler :
        IBaseCommandHandler,
        ICommandHandler<AddCarouselCommandRequestDto, AddCarouselCommandResponseDto>,
        ICommandHandler<EditCarouselCommandRequestDto, EditCarouselCommandResponseDto>,
        ICommandHandler<DeleteCarouselCommandRequestDto, DeleteCarouselCommandResponseDto>
    {

    }
}
