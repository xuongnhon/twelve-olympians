﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class DeletePostCommandRequestDto : CommandRequestDto
    {
        public int Id { get; set; }
    }
}
