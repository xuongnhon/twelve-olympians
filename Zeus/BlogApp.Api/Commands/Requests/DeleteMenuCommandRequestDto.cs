﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class DeleteMenuCommandRequestDto : CommandRequestDto
    {
        public int Id { get; set; }
    }
}
