﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Commands.Requests
{
    public class PopularPostCommandRequestDto : QueryRequestDto
    {
        public int NumberOfPost { get; set; }
    }
}
