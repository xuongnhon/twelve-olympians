﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class EditUserCommandRequestDto : CommandRequestDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
    }
}
