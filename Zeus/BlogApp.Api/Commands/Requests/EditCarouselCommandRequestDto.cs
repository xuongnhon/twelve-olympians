﻿using BlogApp.Api.Infrastructure.Dto.Commands;
using Microsoft.AspNetCore.Http;

namespace BlogApp.Api.Commands.Requests
{
    public class EditCarouselCommandRequestDto : CommandRequestDto
    {
        public int Id { get; set; }
        public IFormFile File { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
    }
}
