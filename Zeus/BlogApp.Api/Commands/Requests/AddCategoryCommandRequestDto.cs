﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class AddCategoryCommandRequestDto : CommandRequestDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
    }
}
