﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class AddMenuCommandRequestDto : CommandRequestDto
    {
        public string Name { get; set; }
    }
}
