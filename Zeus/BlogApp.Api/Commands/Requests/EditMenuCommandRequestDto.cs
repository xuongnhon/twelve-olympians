﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class EditMenuCommandRequestDto : CommandRequestDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
