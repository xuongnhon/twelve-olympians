﻿using BlogApp.Api.Infrastructure.Dto.Commands;
using Microsoft.AspNetCore.Http;

namespace BlogApp.Api.Commands.Requests
{
    public class AddPostCommandRequestDto : CommandRequestDto
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public IFormFile File { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
    }
}
