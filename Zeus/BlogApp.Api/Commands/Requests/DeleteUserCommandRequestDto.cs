﻿using BlogApp.Api.Infrastructure.Dto.Commands;

namespace BlogApp.Api.Commands.Requests
{
    public class DeleteUserCommandRequestDto : CommandRequestDto
    {
        public string Id { get; set; }
    }
}
