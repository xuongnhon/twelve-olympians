﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Exceptions;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Commands
{
    public class CategoryCommandHandler : ICategoryCommandHandler
    {
        private readonly ICategoryService _categoryService;

        public CategoryCommandHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<AddCategoryCommandResponseDto> HandleAsync(AddCategoryCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var category = new CategoryDto
            {
                Name = request.Name,
                Description = request.Description,
                MenuId = request.MenuId
            };

            var result = await _categoryService.Create(category);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new AddCategoryCommandResponseDto
            {
                IsSuccess = true,
                Data = (int)result.Data
            };
        }

        public async Task<EditCategoryCommandResponseDto> HandleAsync(EditCategoryCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var category = new CategoryDto
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                MenuId = request.MenuId
            };

            var result = await _categoryService.Edit(category);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new EditCategoryCommandResponseDto
            {
                IsSuccess = true,
                Data = request.Id
            };
        }

        public async Task<DeleteCategoryCommandResponseDto> HandleAsync(DeleteCategoryCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var result = await _categoryService.Delete(request.Id);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new DeleteCategoryCommandResponseDto
            {
                IsSuccess = true
            };
        }
    }
}
