﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Handlers;

namespace BlogApp.Api.Commands
{
    public interface IUserCommandHandler :
        IBaseCommandHandler,
        ICommandHandler<AddUserCommandRequestDto, AddUserCommandResponseDto>,
        ICommandHandler<EditUserCommandRequestDto, EditUserCommandResponseDto>,
        ICommandHandler<DeleteUserCommandRequestDto, DeleteUserCommandResponseDto>
    {

    }
}
