﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Exceptions;
using BlogApp.Api.Infrastructure.Helpers;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Commands
{
    public class CarouselCommandHandler : ICarouselCommandHandler
    {
        private readonly ICarouselService _carouselService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CarouselCommandHandler(
            ICarouselService carouselService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _carouselService = carouselService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<AddCarouselCommandResponseDto> HandleAsync(AddCarouselCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            string imageUrl;
            try
            {
                imageUrl = await FileHelper.SaveFile(_httpContextAccessor, request.File);
            }
            catch (Exception exception)
            {
                throw new ApiException(exception.Message, inner: exception);
            }

            var carousel = new CarouselDto
            {
                ImageUrl = imageUrl,
                Link = request.Link,
                Title = request.Title,
                Caption = request.Caption
            };

            var result = await _carouselService.Create(carousel);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new AddCarouselCommandResponseDto
            {
                IsSuccess = true,
                Data = (int)result.Data
            };
        }

        public async Task<EditCarouselCommandResponseDto> HandleAsync(EditCarouselCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            string imageUrl;
            if (request.File == null)
            {
                var dbCarousel = await _carouselService.GetById(request.Id);
                if (dbCarousel == null)
                {
                    throw new ApiException(Domain.Common.Constants.Message.CarouselNotFound);
                }
                else
                {
                    imageUrl = dbCarousel.ImageUrl;
                }
            }
            else
            {
                try
                {
                    imageUrl = await FileHelper.SaveFile(_httpContextAccessor, request.File);
                }
                catch (Exception exception)
                {
                    throw new ApiException(exception.Message, inner: exception);
                }
            }

            var carousel = new CarouselDto
            {
                Id = request.Id,
                ImageUrl = imageUrl,
                Link = request.Link,
                Caption = request.Caption,
                Title = request.Title
            };

            var result = await _carouselService.Edit(carousel);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new EditCarouselCommandResponseDto
            {
                IsSuccess = true,
                Data = request.Id
            };
        }

        public async Task<DeleteCarouselCommandResponseDto> HandleAsync(DeleteCarouselCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var result = await _carouselService.Delete(request.Id);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new DeleteCarouselCommandResponseDto
            {
                IsSuccess = true
            };
        }
    }
}
