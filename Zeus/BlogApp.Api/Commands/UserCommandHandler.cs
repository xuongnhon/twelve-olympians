﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Authorization;
using BlogApp.Idp.Api;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Commands
{
    public class UserCommandHandler : IUserCommandHandler
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly UserService.UserServiceClient _userServiceClient;

        public UserCommandHandler(
            ICurrentUserService currentUserService,
            UserService.UserServiceClient userServiceClient
        )
        {
            _currentUserService = currentUserService;
            _userServiceClient = userServiceClient;
        }

        public async Task<AddUserCommandResponseDto> HandleAsync(AddUserCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var response = await _userServiceClient.CreateUserAsync(new CreateUserRequest
            {
                UserName = request.UserName,
                Password = request.Password,
                RoleId = request.RoleId,
                Name = request.Name
            });

            if (response.StatusCode == StatusCode.Success)
            {
                return new AddUserCommandResponseDto
                {
                    IsSuccess = true,
                    Data = response.Data
                };
            }

            return new AddUserCommandResponseDto
            {
                IsSuccess = false
            };
        }

        public async Task<EditUserCommandResponseDto> HandleAsync(EditUserCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            if (string.Equals(_currentUserService.UserId, request.Id, System.StringComparison.OrdinalIgnoreCase))
            {
                return new EditUserCommandResponseDto
                {
                    IsSuccess = false
                };
            }

            var response = await _userServiceClient.EditUserAsync(new EditUserRequest
            {
                Id = request.Id,
                RoleId = request.RoleId,
                Name = request.Name
            });

            return new EditUserCommandResponseDto
            {
                IsSuccess = response.StatusCode == StatusCode.Success
            };
        }

        public async Task<DeleteUserCommandResponseDto> HandleAsync(DeleteUserCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            if (string.Equals(_currentUserService.UserId, request.Id, System.StringComparison.OrdinalIgnoreCase))
            {
                return new DeleteUserCommandResponseDto
                {
                    IsSuccess = false
                };
            }

            var response = await _userServiceClient.DeleteUserAsync(new DeleteUserRequest
            {
                Id = request.Id
            });

            return new DeleteUserCommandResponseDto
            {
                IsSuccess = response.StatusCode == StatusCode.Success
            };
        }
    }
}
