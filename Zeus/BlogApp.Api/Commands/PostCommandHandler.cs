﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Exceptions;
using BlogApp.Api.Infrastructure.Helpers;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;
using BlogApp.Api.Infrastructure.Authorization;

namespace BlogApp.Api.Commands
{
    public class PostCommandHandler : IPostCommandHandler
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IPostService _postService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PostCommandHandler(
            ICurrentUserService currentUserService,
            IPostService postService,
            IHttpContextAccessor httpContextAccessor
        )
        {
            _currentUserService = currentUserService;
            _postService = postService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<AddPostCommandResponseDto> HandleAsync(AddPostCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            string thumbnailImageUrl;
            try
            {
                thumbnailImageUrl = await FileHelper.SaveFile(_httpContextAccessor, request.File);
            }
            catch (Exception exception)
            {
                throw new ApiException(exception.Message, inner: exception);
            }

            var post = new PostDto
            {
                Title = request.Title,
                ShortDescription = request.ShortDescription,
                ThumbnailImageUrl = thumbnailImageUrl,
                Content = request.Content,
                CategoryId = request.CategoryId,
                CreatedBy = _currentUserService.UserId
            };

            var result = await _postService.Create(post);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new AddPostCommandResponseDto
            {
                IsSuccess = true,
                Data = (int)result.Data
            };
        }

        public async Task<EditPostCommandResponseDto> HandleAsync(EditPostCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            string thumbnailImageUrl;
            if (request.File == null)
            {
                var dbPost = await _postService.GetById(request.Id);
                if (dbPost == null)
                {
                    throw new ApiException(Domain.Common.Constants.Message.PostNotFound);
                }
                else
                {
                    thumbnailImageUrl = dbPost.ThumbnailImageUrl;
                }
            }
            else
            {
                try
                {
                    thumbnailImageUrl = await FileHelper.SaveFile(_httpContextAccessor, request.File);
                }
                catch (Exception exception)
                {
                    throw new ApiException(exception.Message, inner: exception);
                }
            }

            var post = new PostDto
            {
                Id = request.Id,
                Title = request.Title,
                ShortDescription = request.ShortDescription,
                ThumbnailImageUrl = thumbnailImageUrl,
                Content = request.Content,
                CategoryId = request.CategoryId,
                UpdatedBy = _currentUserService.UserId
            };

            var result = await _postService.Edit(post);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new EditPostCommandResponseDto
            {
                IsSuccess = true,
                Data = request.Id
            };
        }

        public async Task<DeletePostCommandResponseDto> HandleAsync(DeletePostCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var result = await _postService.Delete(request.Id);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new DeletePostCommandResponseDto
            {
                IsSuccess = true
            };
        }
    }
}
