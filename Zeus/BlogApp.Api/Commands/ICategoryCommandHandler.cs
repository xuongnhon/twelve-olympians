﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Handlers;

namespace BlogApp.Api.Commands
{
    public interface ICategoryCommandHandler :
        IBaseCommandHandler,
        ICommandHandler<AddCategoryCommandRequestDto, AddCategoryCommandResponseDto>,
        ICommandHandler<EditCategoryCommandRequestDto, EditCategoryCommandResponseDto>,
        ICommandHandler<DeleteCategoryCommandRequestDto, DeleteCategoryCommandResponseDto>
    {

    }
}
