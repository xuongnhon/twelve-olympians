﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Exceptions;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Commands
{
    public class MenuCommandHandler : IMenuCommandHandler
    {
        private readonly IMenuService _menuService;

        public MenuCommandHandler(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public async Task<AddMenuCommandResponseDto> HandleAsync(AddMenuCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var menu = new MenuDto
            {
                Name = request.Name
            };

            var result = await _menuService.Create(menu);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new AddMenuCommandResponseDto
            {
                IsSuccess = true,
                Data = (int)result.Data
            };
        }

        public async Task<EditMenuCommandResponseDto> HandleAsync(EditMenuCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var menu = new MenuDto
            {
                Id = request.Id,
                Name = request.Name
            };

            var result = await _menuService.Edit(menu);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new EditMenuCommandResponseDto
            {
                IsSuccess = true,
                Data = request.Id
            };
        }

        public async Task<DeleteMenuCommandResponseDto> HandleAsync(DeleteMenuCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var result = await _menuService.Delete(request.Id);

            if (result.Status != Domain.Common.Enum.ActionStatus.Success)
            {
                throw new ApiException(result.Message);
            }

            return new DeleteMenuCommandResponseDto
            {
                IsSuccess = true
            };
        }
    }
}
