﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class AddCategoryCommandRequestDtoValidator : AbstractValidator<AddCategoryCommandRequestDto>
    {
        public AddCategoryCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .MinimumLength(1)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Description)
                .NotNull()
                .MinimumLength(1)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.MenuId)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
