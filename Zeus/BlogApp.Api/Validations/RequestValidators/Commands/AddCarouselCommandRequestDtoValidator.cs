﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class AddCarouselCommandRequestDtoValidator : AbstractValidator<AddCarouselCommandRequestDto>
    {
        public AddCarouselCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.File)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Link)
                .NotNull()
                .MinimumLength(1)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
