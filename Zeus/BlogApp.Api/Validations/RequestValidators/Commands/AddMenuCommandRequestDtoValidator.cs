﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class AddMenuCommandRequestDtoValidator : AbstractValidator<AddMenuCommandRequestDto>
    {
        public AddMenuCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .Length(1, 100)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
