﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;
using System.Text.RegularExpressions;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class AddUserCommandRequestDtoValidator : AbstractValidator<AddUserCommandRequestDto>
    {
        public AddUserCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.UserName)
                .NotNull()
                .Length(1, 20)
                .Must(userName =>
                {
                    return !userName.Contains(" ");
                })
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Password)
                .NotNull()
                .Length(1, 25)
                .Must(userName =>
                {
                    var regex = new Regex("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,25}$");
                    var result = regex.Match(userName);
                    return result.Success;
                })
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .Length(1, 100)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.RoleId)
                .GreaterThan(0)
                .LessThan(3)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
