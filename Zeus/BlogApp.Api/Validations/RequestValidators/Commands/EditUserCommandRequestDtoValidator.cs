﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class EditUserCommandRequestDtoValidator : AbstractValidator<EditUserCommandRequestDto>
    {
        public EditUserCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Id)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .Length(1, 100)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.RoleId)
                .GreaterThan(0)
                .LessThan(3)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
