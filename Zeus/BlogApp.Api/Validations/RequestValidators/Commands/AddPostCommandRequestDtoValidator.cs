﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class AddPostCommandRequestDtoValidator : AbstractValidator<AddPostCommandRequestDto>
    {
        public AddPostCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.File)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Title)
                .NotNull()
                .Length(1, 100)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.ShortDescription)
                .NotNull()
                .Length(1, 1500)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.CategoryId)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Content)
                .NotNull()
                .MinimumLength(1)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
