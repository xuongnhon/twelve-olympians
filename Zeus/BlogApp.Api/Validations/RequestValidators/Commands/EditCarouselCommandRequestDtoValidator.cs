﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Common.Constants;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Commands
{
    public class EditCarouselCommandRequestDtoValidator : AbstractValidator<EditCarouselCommandRequestDto>
    {
        public EditCarouselCommandRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Id)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Link)
                .NotNull()
                .MinimumLength(1)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
