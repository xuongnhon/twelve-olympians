﻿using BlogApp.Api.Common.Constants;
using BlogApp.Api.Queries.Requests;
using FluentValidation;

namespace BlogApp.Api.Validations.RequestValidators.Queries
{
    public class GetPostsQueryRequestDtoValidator : AbstractValidator<GetPostsQueryRequestDto>
    {
        public GetPostsQueryRequestDtoValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Page)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.ItemsPerPage)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
