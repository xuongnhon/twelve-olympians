﻿using System;
using System.Linq;

namespace BlogApp.Api.Validations.Common
{
    public static class EnumValidator
    {
        private const int UNKNOWN_VALUE = 0;

        public static bool IsValidAndNotUnknown<T>(int enumValue)
        {
            var enumTypeRange = Enum.GetValues(typeof(T)).Cast<int>().ToList();

            return enumValue != UNKNOWN_VALUE && enumTypeRange.Contains(enumValue);
        }
    }
}
