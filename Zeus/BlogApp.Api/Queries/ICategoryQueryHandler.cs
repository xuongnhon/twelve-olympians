﻿using BlogApp.Api.Infrastructure.Handlers;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;

namespace BlogApp.Api.Queries
{
    public interface ICategoryQueryHandler :
        IBaseQueryHandler,
        IQueryHandler<GetCategoriesQueryRequestDto, GetCategoriesQueryResponseDto>,
        IQueryHandler<GetCategoryByIdQueryRequestDto, GetCategoryByIdQueryResponseDto>
    {

    }
}
