﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class GetCarouselsQueryRequestDto : QueryRequestDto
    {
        public string Page { get; set; }
    }
}
