using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class GetCategoryAndPostByMenuIdQueryRequestDto : QueryRequestDto
    {
        public int MenuId { get; set; }
    }
}