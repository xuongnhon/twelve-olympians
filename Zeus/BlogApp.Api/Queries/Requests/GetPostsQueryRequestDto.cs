﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class GetPostsQueryRequestDto : QueryRequestDto
    {
        public string SearchValue { get; set; }
        public string SortBy { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
