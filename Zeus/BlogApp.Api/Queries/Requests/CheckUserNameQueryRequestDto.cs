﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class CheckUserNameQueryRequestDto : QueryRequestDto
    {
        public string UserName { get; set; }
    }
}
