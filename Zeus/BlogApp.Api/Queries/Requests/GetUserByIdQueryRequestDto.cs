﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class GetUserByIdQueryRequestDto : QueryRequestDto
    {
        public string Id { get; set; }
    }
}
