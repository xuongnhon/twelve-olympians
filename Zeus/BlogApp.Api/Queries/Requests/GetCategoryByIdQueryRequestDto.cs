﻿using BlogApp.Api.Infrastructure.Dto.Queries;

namespace BlogApp.Api.Queries.Requests
{
    public class GetCategoryByIdQueryRequestDto : QueryRequestDto
    {
        public int Id { get; set; }
    }
}
