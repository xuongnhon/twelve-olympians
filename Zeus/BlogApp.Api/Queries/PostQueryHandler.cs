﻿using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;
using BlogApp.Idp.Api;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Commands.Requests;

namespace BlogApp.Api.Queries
{
    public class PostQueryHandler : IPostQueryHandler
    {
        private readonly UserService.UserServiceClient _userServiceClient;
        private readonly IPostService _postService;

        public PostQueryHandler(
            UserService.UserServiceClient userServiceClient,
            IPostService postService
        )
        {
            _userServiceClient = userServiceClient;
            _postService = postService;
        }

        public async Task<GetPostsQueryResponseDto> HandleAsync(GetPostsQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var allUsers = await _userServiceClient.GetUsersAsync(new GetUsersRequest());
            var result = await _postService.GetPosts(
                request.SearchValue,
                request.SortBy,
                request.IsDescending,
                page: request.Page,
                itemsPerPage: request.ItemsPerPage
            );

            foreach (var post in result.Data)
            {
                post.CreatedBy = allUsers.Data.First(x => x.Id == post.CreatedBy).Name;
            }

            return new GetPostsQueryResponseDto
            {
                IsSuccess = true,
                Data = result.Data,
                Total = result.Total,
                Page = result.Page,
                ItemsPerPage = result.ItemsPerPage
            };
        }

        public async Task<GetPostByIdQueryResponseDto> HandleAsync(GetPostByIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var post = await _postService.GetById(request.Id);
            var creator = await _userServiceClient.GetUserByIdAsync(new GetUserByIdRequest
            {
                Id = post.CreatedBy
            });
            post.CreatedBy = creator.Data.Name;

            return new GetPostByIdQueryResponseDto
            {
                IsSuccess = post != null,
                Data = post
            };
        }

        public async Task<PopularPostCommandResponseDto> HandleAsync(PopularPostCommandRequestDto request, CancellationToken cancellationToken = default)
        {
            var post = await _postService.GetPopularPost(request.NumberOfPost);

            return new PopularPostCommandResponseDto
            {
                IsSuccess = post != null,
                Data = post
            };
        }
    }
}
