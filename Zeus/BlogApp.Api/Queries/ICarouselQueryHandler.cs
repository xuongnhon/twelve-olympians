﻿using BlogApp.Api.Infrastructure.Handlers;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;

namespace BlogApp.Api.Queries
{
    public interface ICarouselQueryHandler :
        IBaseQueryHandler,
        IQueryHandler<GetCarouselsQueryRequestDto, GetCarouselsQueryResponseDto>,
        IQueryHandler<GetCarouselByIdQueryRequestDto, GetCarouselByIdQueryResponseDto>
    {

    }
}
