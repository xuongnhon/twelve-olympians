﻿using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;
using BlogApp.Api.Domain.ServiceContracts;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Queries
{
    public class CategoryQueryHandler : ICategoryQueryHandler
    {
        private readonly ICategoryService _categoryService;

        public CategoryQueryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<GetCategoriesQueryResponseDto> HandleAsync(GetCategoriesQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var categories = await _categoryService.GetAll();

            return new GetCategoriesQueryResponseDto
            {
                IsSuccess = true,
                Data = categories
            };
        }

        public async Task<GetCategoryByIdQueryResponseDto> HandleAsync(GetCategoryByIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var category = await _categoryService.GetById(request.Id);

            return new GetCategoryByIdQueryResponseDto
            {
                IsSuccess = category != null,
                Data = category
            };
        }
    }
}
