﻿using BlogApp.Api.Collaborators.IdentityProvider.Dto;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;
using BlogApp.Idp.Api;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Queries
{
    public class UserQueryHandler : IUserQueryHandler
    {
        private readonly UserService.UserServiceClient _userServiceClient;

        public UserQueryHandler(
            UserService.UserServiceClient userServiceClient
        )
        {
            _userServiceClient = userServiceClient;
        }

        public async Task<GetUsersQueryResponseDto> HandleAsync(GetUsersQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var response = await _userServiceClient.GetUsersAsync(new GetUsersRequest());

            if (response.StatusCode == StatusCode.Success)
            {
                var users = response.Data.Select(user => new UserDetailDto
                {
                    Id = user.Id,
                    Name = user.Name,
                    RoleId = user.RoleId,
                    RoleName = user.RoleName,
                    UserName = user.UserName
                })
                .ToList();

                return new GetUsersQueryResponseDto
                {
                    IsSuccess = true,
                    Data = users
                };
            }

            return new GetUsersQueryResponseDto
            {
                IsSuccess = false
            };
        }

        public async Task<CheckUserNameQueryResponseDto> HandleAsync(CheckUserNameQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var response = await _userServiceClient.CheckUserNameAsync(new CheckUserNameRequest
            {
                UserName = request.UserName
            });

            if (response.StatusCode == StatusCode.Success)
            {
                return new CheckUserNameQueryResponseDto
                {
                    IsSuccess = true,
                    Data = response.Data
                };
            }

            return new CheckUserNameQueryResponseDto
            {
                IsSuccess = false
            };
        }

        public async Task<GetUserByIdQueryResponseDto> HandleAsync(GetUserByIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var response = await _userServiceClient.GetUserByIdAsync(new GetUserByIdRequest
            {
                Id = request.Id
            });

            if (response.StatusCode == StatusCode.Success)
            {
                var user = new UserDetailDto
                {
                    Id = response.Data.Id,
                    Name = response.Data.Name,
                    RoleId = response.Data.RoleId,
                    RoleName = response.Data.RoleName,
                    UserName = response.Data.UserName
                };

                return new GetUserByIdQueryResponseDto
                {
                    IsSuccess = true,
                    Data = user
                };
            }

            return new GetUserByIdQueryResponseDto
            {
                IsSuccess = false
            };
        }
    }
}
