﻿using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Commands.Responses;
using BlogApp.Api.Infrastructure.Handlers;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;

namespace BlogApp.Api.Queries
{
    public interface IPostQueryHandler :
        IBaseQueryHandler,
        IPaginatedQueryHandler<GetPostsQueryRequestDto, GetPostsQueryResponseDto>,
        IQueryHandler<GetPostByIdQueryRequestDto, GetPostByIdQueryResponseDto>,
        IQueryHandler<PopularPostCommandRequestDto, PopularPostCommandResponseDto>
    {
    }
}
