﻿using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;
using BlogApp.Api.Domain.ServiceContracts;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Queries
{
    public class MenuQueryHandler : IMenuQueryHandler
    {
        private readonly IMenuService _menuService;

        public MenuQueryHandler(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public async Task<GetMenusQueryResponseDto> HandleAsync(GetMenusQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var menus = await _menuService.GetAll();

            return new GetMenusQueryResponseDto
            {
                IsSuccess = true,
                Data = menus
            };
        }

        public async Task<GetMenuByIdQueryResponseDto> HandleAsync(GetMenuByIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var menu = await _menuService.GetById(request.Id);

            return new GetMenuByIdQueryResponseDto
            {
                IsSuccess = menu != null,
                Data = menu
            };
        }

        public async Task<GetCategoryAndPostByMenuIdResponseDto> HandleAsync(GetCategoryAndPostByMenuIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var categories = await _menuService.GetCategoryAndPostByMenuId(request.MenuId);

            return new GetCategoryAndPostByMenuIdResponseDto
            {
                IsSuccess = categories != null,
                Data = categories
            };
        }
    }
}
