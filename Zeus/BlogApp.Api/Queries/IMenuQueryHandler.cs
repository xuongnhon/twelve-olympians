﻿using BlogApp.Api.Infrastructure.Handlers;
using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;

namespace BlogApp.Api.Queries
{
    public interface IMenuQueryHandler :
        IBaseQueryHandler,
        IQueryHandler<GetMenusQueryRequestDto, GetMenusQueryResponseDto>,
        IQueryHandler<GetMenuByIdQueryRequestDto, GetMenuByIdQueryResponseDto>,
        IQueryHandler<GetCategoryAndPostByMenuIdQueryRequestDto, GetCategoryAndPostByMenuIdResponseDto>
    {

    }
}
