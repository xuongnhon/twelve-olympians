﻿using BlogApp.Api.Queries.Requests;
using BlogApp.Api.Queries.Responses;
using BlogApp.Api.Domain.ServiceContracts;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Queries
{
    public class CarouselQueryHandler : ICarouselQueryHandler
    {
        private readonly ICarouselService _carouselService;

        public CarouselQueryHandler(ICarouselService carouselService)
        {
            _carouselService = carouselService;
        }

        public async Task<GetCarouselsQueryResponseDto> HandleAsync(GetCarouselsQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var carousels = await _carouselService.GetAll();

            return new GetCarouselsQueryResponseDto
            {
                IsSuccess = true,
                Data = carousels
            };
        }

        public async Task<GetCarouselByIdQueryResponseDto> HandleAsync(GetCarouselByIdQueryRequestDto request, CancellationToken cancellationToken = default)
        {
            var carousel = await _carouselService.GetById(request.Id);

            return new GetCarouselByIdQueryResponseDto
            {
                IsSuccess = carousel != null,
                Data = carousel
            };
        }
    }
}
