﻿using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpGet("~/api/comments/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _commentService.GetByPostId(id);
            return Ok(result);
        }

        [Authorize]
        [HttpPost("~/api/comments/add")]
        public async Task<IActionResult> Create([FromBody] CommentDto commentDto)
        {
            var result = await _commentService.Add(commentDto);
            return Ok(result);
        }

        [Authorize(Roles = "admin")]
        [HttpPost("~/api/comments/delete")]
        public async Task<IActionResult> Delete([FromBody] CommentDto commentDto)
        {
            var result = await _commentService.Add(commentDto);
            return Ok(result);
        }
    }
}
