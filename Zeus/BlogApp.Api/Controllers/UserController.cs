﻿using BlogApp.Api.Commands;
using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Queries;
using BlogApp.Api.Queries.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserCommandHandler _userCommandHandler;
        private readonly IUserQueryHandler _userQueryHandler;

        public UserController(
            IUserCommandHandler userCommandHandler,
            IUserQueryHandler userQueryHandler
        )
        {
            _userCommandHandler = userCommandHandler;
            _userQueryHandler = userQueryHandler;
        }

        [HttpGet("~/api/users")]
        public async Task<IActionResult> Get(CancellationToken cancellationToken)
        {
            var result = await _userQueryHandler.HandleAsync(new GetUsersQueryRequestDto(), cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/users/{id}")]
        public async Task<IActionResult> Get(string id, CancellationToken cancellationToken)
        {
            var result = await _userQueryHandler.HandleAsync(new GetUserByIdQueryRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/users/check/{userName}")]
        public async Task<IActionResult> CheckUserName(string userName, CancellationToken cancellationToken)
        {
            var result = await _userQueryHandler.HandleAsync(new CheckUserNameQueryRequestDto
            {
                UserName = userName
            }, cancellationToken);

            return Ok(result);
        }

        [HttpPost("~/api/users/add")]
        public async Task<IActionResult> Add([FromBody] AddUserCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _userCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpPut("~/api/users/edit")]
        public async Task<IActionResult> Edit([FromBody] EditUserCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _userCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpDelete("~/api/users/delete/{id}")]
        public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
        {
            var result = await _userCommandHandler.HandleAsync(new DeleteUserCommandRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }
    }
}
