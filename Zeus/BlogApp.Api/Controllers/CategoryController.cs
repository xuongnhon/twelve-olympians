﻿using BlogApp.Api.Commands;
using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Queries;
using BlogApp.Api.Queries.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class CategoryController : BlogAppController
    {
        private readonly ICategoryCommandHandler _categoryCommandHandler;
        private readonly ICategoryQueryHandler _categoryQueryHandler;

        public CategoryController(
            ICategoryCommandHandler categoryCommandHandler,
            ICategoryQueryHandler categoryQueryHandler
        )
        {
            _categoryCommandHandler = categoryCommandHandler;
            _categoryQueryHandler = categoryQueryHandler;
        }

        [HttpGet("~/api/categories")]
        public async Task<IActionResult> Get(CancellationToken cancellationToken)
        {
            var result = await _categoryQueryHandler.HandleAsync(new GetCategoriesQueryRequestDto(), cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/categories/{id}")]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            var result = await _categoryQueryHandler.HandleAsync(new GetCategoryByIdQueryRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [HttpPost("~/api/categories/add")]
        public async Task<IActionResult> Add([FromBody] AddCategoryCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _categoryCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpPut("~/api/categories/edit")]
        public async Task<IActionResult> Edit([FromBody] EditCategoryCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _categoryCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpDelete("~/api/categories/delete/{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            var result = await _categoryCommandHandler.HandleAsync(new DeleteCategoryCommandRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }
    }
}
