﻿using BlogApp.Api.Commands;
using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Queries;
using BlogApp.Api.Queries.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class PostController : BlogAppController
    {
        private readonly IPostCommandHandler _postCommandHandler;
        private readonly IPostQueryHandler _postQueryHandler;
        private readonly IPostService _postService;

        public PostController(
            IPostCommandHandler postCommandHandler,
            IPostQueryHandler postQueryHandler,
            IPostService postService
        )
        {
            _postCommandHandler = postCommandHandler;
            _postQueryHandler = postQueryHandler;
            _postService = postService;
        }

        [HttpGet("~/api/posts")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromQuery] GetPostsQueryRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _postQueryHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("~/api/getposts")]
        public async Task<IActionResult> GetPosts([FromQuery] GetPostsQueryRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _postQueryHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/posts/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            var result = await _postQueryHandler.HandleAsync(new GetPostByIdQueryRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("~/api/posts/GetByCategoryId/{categoryId}/{postId}")]
        public async Task<IActionResult> GetByCategoryId(int categoryId, int postId, CancellationToken cancellationToken)
        {
            var result = await _postService.GetPostsByCategoryId(categoryId, postId);

            return Ok(result);
        }

        [HttpPost("~/api/posts/add")]
        public async Task<IActionResult> Add(AddPostCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _postCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpPut("~/api/posts/edit")]
        public async Task<IActionResult> Edit(EditPostCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _postCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpDelete("~/api/posts/delete/{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            var result = await _postCommandHandler.HandleAsync(new DeletePostCommandRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("~/api/posts/popularPosts")]
        public async Task<IActionResult> GetPopularPosts(int numberOfPost, CancellationToken cancellationToken)
        {
            var result = await _postQueryHandler.HandleAsync(new PopularPostCommandRequestDto() { 
                NumberOfPost = numberOfPost
            }, cancellationToken);

            return Ok(result);
        }
    }
}
