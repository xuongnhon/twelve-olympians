﻿using BlogApp.Api.Commands;
using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Queries;
using BlogApp.Api.Queries.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class MenuController : BlogAppController
    {
        private readonly IMenuCommandHandler _menuCommandHandler;
        private readonly IMenuQueryHandler _menuQueryHandler;

        public MenuController(
            IMenuCommandHandler menuCommandHandler,
            IMenuQueryHandler menuQueryHandler
        )
        {
            _menuCommandHandler = menuCommandHandler;
            _menuQueryHandler = menuQueryHandler;
        }

        [HttpGet("~/api/menus")]
        public async Task<IActionResult> Get(CancellationToken cancellationToken)
        {
            var result = await _menuQueryHandler.HandleAsync(new GetMenusQueryRequestDto(), cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/getmenus")]
        [AllowAnonymous]
        public async Task<IActionResult> GetMenus(CancellationToken cancellationToken)
        {
            var result = await _menuQueryHandler.HandleAsync(new GetMenusQueryRequestDto(), cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/menus/{id}")]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            var result = await _menuQueryHandler.HandleAsync(new GetMenuByIdQueryRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [HttpPost("~/api/menus/add")]
        public async Task<IActionResult> Add([FromBody] AddMenuCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _menuCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpPut("~/api/menus/edit")]
        public async Task<IActionResult> Edit([FromBody] EditMenuCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _menuCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpDelete("~/api/menus/delete/{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            var result = await _menuCommandHandler.HandleAsync(new DeleteMenuCommandRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }
        [AllowAnonymous]
        [HttpGet("~/api/menus/categories/{id}")]
        public async Task<IActionResult> GetCategoryAndPostById(int id, CancellationToken cancellationToken)
        {
            var result = await _menuQueryHandler.HandleAsync(new GetCategoryAndPostByMenuIdQueryRequestDto
            {
                MenuId = id
            }, cancellationToken);

            return Ok(result);
        }
    }
}
