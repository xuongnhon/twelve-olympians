﻿using BlogApp.Api.Commands;
using BlogApp.Api.Commands.Requests;
using BlogApp.Api.Queries;
using BlogApp.Api.Queries.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    public class CarouselController : BlogAppController
    {
        private readonly ICarouselCommandHandler _carouselCommandHandler;
        private readonly ICarouselQueryHandler _carouselQueryHandler;

        public CarouselController(
            ICarouselCommandHandler carouselCommandHandler,
            ICarouselQueryHandler carouselQueryHandler
        )
        {
            _carouselCommandHandler = carouselCommandHandler;
            _carouselQueryHandler = carouselQueryHandler;
        }

        [HttpGet("~/api/carousels")]
        public async Task<IActionResult> Get(CancellationToken cancellationToken)
        {
            var result = await _carouselQueryHandler.HandleAsync(new GetCarouselsQueryRequestDto(), cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/carousels/getByPage")]
        [AllowAnonymous]
        public async Task<IActionResult> getByPage(string page, CancellationToken cancellationToken)
        {
            var result = await _carouselQueryHandler.HandleAsync(new GetCarouselsQueryRequestDto()
            {
                Page = page
            }, cancellationToken);

            return Ok(result);
        }

        [HttpGet("~/api/carousels/{id}")]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            var result = await _carouselQueryHandler.HandleAsync(new GetCarouselByIdQueryRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }

        [HttpPost("~/api/carousels/add")]
        public async Task<IActionResult> Add(AddCarouselCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _carouselCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpPut("~/api/carousels/edit")]
        public async Task<IActionResult> Edit(EditCarouselCommandRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _carouselCommandHandler.HandleAsync(request, cancellationToken);

            return Ok(result);
        }

        [HttpDelete("~/api/carousels/delete/{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            var result = await _carouselCommandHandler.HandleAsync(new DeleteCarouselCommandRequestDto
            {
                Id = id
            }, cancellationToken);

            return Ok(result);
        }
    }
}
