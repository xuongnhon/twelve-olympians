﻿using BlogApp.Api.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BlogApp.Api.Controllers
{
    [Authorize(Roles = "admin")]
    public abstract class BlogAppController : Controller
    {
        private readonly List<string> _allowedExtentions = new List<string> { "png", "jpg" };

        protected async Task<string> SaveFile(IHttpContextAccessor httpContextAccessor, IFormFile file)
        {
            var folderPath = $@"{Directory.GetCurrentDirectory()}\wwwroot\images";
            var splitValues = Path.GetFileName(file.FileName).Split('.');
            if (splitValues.Length > 1)
            {
                var extension = splitValues[splitValues.Length - 1].ToLower();
                if (_allowedExtentions.Contains(extension))
                {
                    var fileName = $"{Guid.NewGuid()}.{extension}";
                    var request = httpContextAccessor.HttpContext.Request;
                    var fileUrl = $@"{request.Scheme}://{request.Host}/images/{fileName}";
                    var filePath = Path.Combine(folderPath, fileName);

                    using (var memoryStream = new MemoryStream())
                    {
                        file.OpenReadStream().CopyTo(memoryStream);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        await System.IO.File.WriteAllBytesAsync(filePath, memoryStream.ToArray());
                    }

                    return fileUrl;
                }
            }

            throw new ApiException("Invalid file!");
        }
    }
}
