﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.MessageHandlers
{
    public class GrpcMessageHandler : DelegatingHandler
    {
        private readonly Action<HttpRequestHeaders> _forwardingHeaderFunc;

        public GrpcMessageHandler(Action<HttpRequestHeaders> forwardingHeaderFunc)
        {
            _forwardingHeaderFunc = forwardingHeaderFunc;
        }

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            _forwardingHeaderFunc?.Invoke(request.Headers);
            return base.SendAsync(request, cancellationToken);
        }
    }
}
