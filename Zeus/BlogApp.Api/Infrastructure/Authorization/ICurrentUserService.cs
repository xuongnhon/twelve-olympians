﻿namespace BlogApp.Api.Infrastructure.Authorization
{
    public interface ICurrentUserService
    {
        string UserId { get; }
    }
}
