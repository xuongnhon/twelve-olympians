﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace BlogApp.Api.Infrastructure.Authorization
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor?
                .HttpContext?
                .User?
                .Claims?
                .FirstOrDefault(claim => claim.Type == "sub")?
                .Value;
        }

        public string UserId { get; }
    }
}
