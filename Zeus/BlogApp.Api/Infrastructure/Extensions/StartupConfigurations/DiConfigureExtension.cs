﻿using BlogApp.Api.Infrastructure.Authorization;
using BlogApp.Api.Infrastructure.Config;
using BlogApp.Api.Infrastructure.Handlers;
using BlogApp.Api.Infrastructure.MessageHandlers;
using BlogApp.Api.Infrastructure.Utils;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Idp.Api;
using BlogApp.Api.Infrastructure.DataAccess;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogApp.Api.Infrastructure.Extensions.StartupConfigurations
{
    public static class DiConfigureExtension
    {
        public static IServiceCollection RegisterApplicationDependencyInjection(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var commandHandlers = TypeHelper.FindClassesOfType<IBaseCommandHandler>(assemblies);
            foreach (var commandHandler in commandHandlers)
            {
                var commandHandlerInterface = commandHandler.GetInterfaces().First();
                services.AddScoped(commandHandlerInterface, commandHandler);
            }

            var queryHandlers = TypeHelper.FindClassesOfType<IBaseQueryHandler>(assemblies);
            foreach (var queryHandler in queryHandlers)
            {
                var queryHandlerInterface = queryHandler.GetInterfaces().First();
                services.AddScoped(queryHandlerInterface, queryHandler);
            }

            var domainServices = TypeHelper.FindClassesOfType<IBaseService>(assemblies);
            foreach (var domainService in domainServices)
            {
                var domainServiceInterface = domainService.GetInterfaces().First();
                services.AddScoped(domainServiceInterface, domainService);
            }

            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            return services;
        }

        public static IServiceCollection AddGrpcClient(this IServiceCollection services, IConfigurationSection configurationSection)
        {
            if (configurationSection == null)
            {
                throw new ArgumentNullException(nameof(configurationSection), "Please config GRPC section");
            }

            var grpcApiConfig = configurationSection.Get<GrpcApiConfig>();

            return services.AddAuditLogServiceGrpc(grpcApiConfig.IdpService);
        }

        private static IServiceCollection AddAuditLogServiceGrpc(this IServiceCollection services, GrpcHostConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config), "Please config GRPC.AuditLogService");
            }

            AddGrpcClient<UserService.UserServiceClient>(services, config.Host, config.SslMode, config.Headers);

            return services;
        }

        private static void AddGrpcClient<TClient>(
            IServiceCollection services,
            string url,
            bool isSsl,
            IDictionary<string, string> headers = null) where TClient : ClientBase
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException(nameof(url), "Host should not empty.");
            }

            var httpClientBuilder = services.AddGrpcClient<TClient>(x => x.Address = new Uri(url));

            if (isSsl)
            {
                httpClientBuilder.ConfigureChannel(channel => channel.Credentials = new SslCredentials());
            }

            if (headers != null && headers.Any())
            {
                httpClientBuilder.AddHttpMessageHandler((serviceProvider) => new GrpcMessageHandler((h) =>
                {
                    foreach (var header in headers)
                    {
                        h.Add(header.Key, header.Value);
                    }
                }));
            }
        }
    }
}
