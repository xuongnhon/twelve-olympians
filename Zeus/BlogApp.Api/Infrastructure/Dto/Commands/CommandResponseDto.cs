﻿namespace BlogApp.Api.Infrastructure.Dto.Commands
{
    public class CommandResponseDto
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
    }
}
