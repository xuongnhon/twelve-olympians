﻿namespace BlogApp.Api.Infrastructure.Dto.Queries
{
    public class QueryResponseDto
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
    }
}
