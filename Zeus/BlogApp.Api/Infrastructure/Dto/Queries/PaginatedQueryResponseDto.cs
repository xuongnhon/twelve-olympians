﻿namespace BlogApp.Api.Infrastructure.Dto.Queries
{
    public class PaginatedQueryResponseDto
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
        public int Total { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
