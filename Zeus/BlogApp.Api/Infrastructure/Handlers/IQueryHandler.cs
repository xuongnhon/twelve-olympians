﻿using BlogApp.Api.Infrastructure.Dto.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.Handlers
{
    public interface IQueryHandler<TRequestDto, TResponseDto>
        where TRequestDto : QueryRequestDto
        where TResponseDto : QueryResponseDto
    {
        Task<TResponseDto> HandleAsync(TRequestDto request, CancellationToken cancellationToken = default);
    }
}
