﻿using BlogApp.Api.Infrastructure.Dto.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.Handlers
{
    public interface IPaginatedQueryHandler<TRequestDto, TResponseDto>
        where TRequestDto : QueryRequestDto
        where TResponseDto : PaginatedQueryResponseDto
    {
        Task<TResponseDto> HandleAsync(TRequestDto request, CancellationToken cancellationToken = default);
    }
}
