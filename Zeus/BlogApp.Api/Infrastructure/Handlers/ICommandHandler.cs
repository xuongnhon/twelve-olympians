﻿using BlogApp.Api.Infrastructure.Dto.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.Handlers
{
    public interface ICommandHandler<TRequestDto, TResponseDto>
        where TRequestDto : CommandRequestDto
        where TResponseDto : CommandResponseDto
    {
        Task<TResponseDto> HandleAsync(TRequestDto request, CancellationToken cancellationToken = default);
    }
}
