﻿using System.Collections.Generic;

namespace BlogApp.Api.Infrastructure.Config
{
    public class GrpcHostConfig
    {
        public string Host { get; set; }

        public bool SslMode { get; set; }

        public IDictionary<string, string> Headers { get; set; }
    }
}
