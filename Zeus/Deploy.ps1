Write-Host "Octopus parameters:"
Write-Host "	ZeusDeployFolderPath=$ZeusDeployFolderPath"
Write-Host "	ZeusServiceName=$ZeusServiceName"
Write-Host "	ZeusIdentityUrl=$ZeusIdentityUrl"
Write-Host "	ZeusGrpcIdpServiceHost=$ZeusGrpcIdpServiceHost"
Write-Host "	ZeusHttpUrl=$ZeusHttpUrl"


If (Get-Service $ZeusServiceName -ErrorAction SilentlyContinue) {
    If ((Get-Service $ZeusServiceName).Status -eq 'Running') {
		Write-Host "Stop running service."
        Stop-Service $ZeusServiceName
        Write-Host "Service stopped."
    }
	
	Write-Host "Removing service..."
	sc.exe delete $ZeusServiceName
	Write-Host "Done!"
}


$currentFolder = Get-Location
Write-Host "Copying files..."
New-Item -ItemType Directory -Force -Path "$ZeusDeployFolderPath\Zeus"
Copy-Item -Path "$currentFolder\Zeus" -Recurse -Destination $ZeusDeployFolderPath -Container -Force
New-Item -ItemType Directory -Force -Path "$ZeusDeployFolderPath\Zeus\wwwroot"
Write-Host "Done!"


Write-Host "Updating configuration file..."
$config = Get-Content "$ZeusDeployFolderPath\Zeus\appsettings.json" | ConvertFrom-Json 
$config.IdentityUrl = $ZeusIdentityUrl
$config.Grpc.IdpService.Host = $ZeusGrpcIdpServiceHost
$config.Kestrel.Endpoints.Http.Url = $ZeusHttpUrl
$config | ConvertTo-Json -Depth 10 | Out-File "$ZeusDeployFolderPath\Zeus\appsettings.json"
Write-Host "Done!"


Write-Host "Installing service..."
sc.exe create $ZeusServiceName binPath="$ZeusDeployFolderPath\Zeus\BlogApp.Api.exe"
sc.exe start $ZeusServiceName
Write-Host "Done!"