﻿namespace BlogApp.Api.Domain.Common.Constants
{
    public static class Message
    {
        public const string ErrorMessage = "Sorry an error has occurred, please try again later.";

        public const string CreateMenuSuccessfully = "Create menu successfully!";
        public const string EditMenuSuccessfully = "Edit menu successfully!";
        public const string DeleteMenuSuccessfully = "Delete menu successfully!";
        public const string MenuNotFound = "Menu could not be found!";

        public const string CreateCarouselSuccessfully = "Create slider successfully!";
        public const string EditCarouselSuccessfully = "Edit slider successfully!";
        public const string DeleteCarouselSuccessfully = "Delete slider successfully!";
        public const string CarouselNotFound = "Carousel could not be found!";

        public const string CreateCategorySuccessfully = "Create category successfully!";
        public const string EditCategorySuccessfully = "Edit category successfully!";
        public const string DeleteCategorySuccessfully = "Delete category successfully!";
        public const string CategoryNotFound = "Category could not be found!";

        public const string CreatePostSuccessfully = "Create post successfully!";
        public const string EditPostSuccessfully = "Edit post successfully!";
        public const string DeletePostSuccessfully = "Delete post successfully!";
        public const string PostNotFound = "Post could not be found!";

        public const string CreateUserSuccessfully = "Create user successfully!";
        public const string EditUserSuccessfully = "Edit user successfully!";
        public const string DeleteUserSuccessfully = "Delete user successfully!";
        public const string UserNotFound = "User could not be found!";
        public const string UserNameExist = "The username exist!";
        public const string UserNameLoggedIn = "Can not delete this user, because user is currently logged in!";

        public const string LatestPosts = "Latest posts";
    }
}
