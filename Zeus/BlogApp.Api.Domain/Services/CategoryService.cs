﻿using BlogApp.Api.Domain.Common.Constants;
using BlogApp.Api.Domain.Common.Enum;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Infrastructure.DataAccess;
using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<MenuEntity, int> _menuRepository;
        private readonly IRepository<CategoryEntity, int> _categoryRepository;
        private readonly IUnitOfWork _uow;

        public CategoryService(
            IRepository<MenuEntity, int> menuRepository,
            IRepository<CategoryEntity, int> categoryRepository,
            IUnitOfWork uow
        )
        {
            _menuRepository = menuRepository;
            _categoryRepository = categoryRepository;
            _uow = uow;
        }

        public async Task<List<CategoryDetailDto>> GetAll()
        {
            var result = await _categoryRepository.DbSet
                .Select(category => new CategoryDetailDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description,
                    MenuId = category.MenuId,
                    MenuName = category.Menu.Name,
                    NumberOfPosts = category.Posts.Count,
                    CreatedDate = category.CreatedDate
                })
                .ToListAsync();

            return result;
        }

        public async Task<CategoryDetailDto> GetById(int id)
        {
            var result = await _categoryRepository.DbSet
                .Where(category => category.Id == id)
                .Select(category => new CategoryDetailDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description,
                    MenuId = category.MenuId,
                    MenuName = category.Menu.Name,
                    NumberOfPosts = category.Posts.Count,
                    CreatedDate = category.CreatedDate
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(CategoryDto category)
        {
            var menu = await _menuRepository.FindOneAsync(x => x.Id == category.MenuId);
            if (menu != null)
            {
                var newCategory = new CategoryEntity
                {
                    Name = category.Name,
                    Description = category.Description,
                    MenuId = category.MenuId
                };

                _categoryRepository.Add(newCategory);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.CreateCategorySuccessfully,
                    Data = newCategory.Id
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.MenuNotFound
            };
        }

        public async Task<ResultDto> Edit(CategoryDto category)
        {
            var menu = await _menuRepository.FindOneAsync(x => x.Id == category.MenuId);
            if (menu != null)
            {
                var dbCategory = await _categoryRepository.FindOneAsync(x => x.Id == category.Id);
                if (dbCategory != null)
                {
                    dbCategory.Name = category.Name;
                    dbCategory.Description = category.Description;
                    dbCategory.MenuId = category.MenuId;

                    await _uow.CommitChangesAsync();

                    return new ResultDto
                    {
                        Status = ActionStatus.Success,
                        Message = Message.EditCategorySuccessfully
                    };
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.CategoryNotFound
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.MenuNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == id);
            if (category != null)
            {
                _categoryRepository.Delete(category);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteCategorySuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CategoryNotFound
            };
        }
    }
}
