﻿using BlogApp.Api.Domain.Common.Enum;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Infrastructure.DataAccess;
using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.Services
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<PostEntity, int> _postRepository;
        private readonly IRepository<CommentEntity, long> _commentRepository;
        private readonly IUnitOfWork _uow;

        public CommentService(
            IRepository<PostEntity, int> postRepository,
            IRepository<CommentEntity, long> commentRepository,
            IUnitOfWork uow
        )
        {
            _postRepository = postRepository;
            _commentRepository = commentRepository;
            _uow = uow;
        }

        public async Task<ResultDto> Add(CommentDto comment)
        {
            var post = await _postRepository.FindOneAsync(x => x.Id == comment.PostId);
            if (post != null)
            {
                if (comment.CommentId.HasValue)
                {
                    var repliedComment = await _commentRepository.FindOneAsync(x =>
                        x.Id == comment.CommentId.Value
                        && x.PostId == comment.PostId
                    );

                    if (repliedComment == null)
                    {
                        return new ResultDto
                        {
                            Status = ActionStatus.Error
                        };
                    }
                }

                var newComment = new CommentEntity
                {
                    PostId = comment.PostId,
                    CommentId = comment.CommentId,
                    Content = comment.Content,
                    CreatedBy = comment.CommentBy,
                    CreatedDate = DateTime.Now
                };

                _commentRepository.Add(newComment);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error
            };
        }

        public async Task<List<CommentDto>> GetByPostId(int id)
        {
            var dateNow = DateTime.Now;
            var result = await _commentRepository.DbSet.Where(s => s.PostId == id).Select(comment => new CommentDto
            {
                CommentId = comment.CommentId,
                PostId = comment.PostId,
                Content = comment.Content,
                CommentBy = comment.CreatedBy,
                CommentDate = comment.CreatedDate,
                Id = (int)comment.Id
            }).ToListAsync();


            return result;
        }
    }
}
