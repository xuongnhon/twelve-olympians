﻿using BlogApp.Api.Domain.Common.Constants;
using BlogApp.Api.Domain.Common.Enum;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Infrastructure.DataAccess;
using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.Services
{
    public class MenuService : IMenuService
    {
        private readonly IRepository<MenuEntity, int> _menuRepository;
        private readonly IUnitOfWork _uow;

        public MenuService(
            IRepository<MenuEntity, int> menuRepository,
            IUnitOfWork uow
        )
        {
            _menuRepository = menuRepository;
            _uow = uow;
        }

        public async Task<List<MenuDetailDto>> GetAll()
        {
            var result = await _menuRepository.DbSet
                .Select(menu => new MenuDetailDto
                {
                    Id = menu.Id,
                    Name = menu.Name,
                    NumberOfCategories = menu.Categories.Count,
                    NumberOfPosts = menu.Categories.Select(category => category.Posts.Count).Sum(),
                    CreatedDate = menu.CreatedDate
                })
                .ToListAsync();

            return result;
        }

        public async Task<MenuDetailDto> GetById(int id)
        {
            var result = await _menuRepository.DbSet
                .Where(menu => menu.Id == id)
                .Select(menu => new MenuDetailDto
                {
                    Id = menu.Id,
                    Name = menu.Name,
                    NumberOfCategories = menu.Categories.Count,
                    NumberOfPosts = menu.Categories.Select(category => category.Posts.Count).Sum(),
                    CreatedDate = menu.CreatedDate
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(MenuDto menu)
        {
            var newMenu = new MenuEntity
            {
                Name = menu.Name
            };

            _menuRepository.Add(newMenu);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CreateMenuSuccessfully,
                Data = newMenu.Id
            };
        }

        public async Task<ResultDto> Edit(MenuDto menu)
        {
            var dbMenu = await _menuRepository.FindOneAsync(x => x.Id == menu.Id);
            if (dbMenu != null)
            {
                dbMenu.Name = menu.Name;

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.EditMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.MenuNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var menu = await _menuRepository.FindOneAsync(x => x.Id == id);
            if (menu != null)
            {
                _menuRepository.Delete(menu);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.MenuNotFound
            };
        }

        public async Task<CategoriesOfMenuDto> GetCategoryAndPostByMenuId(int menuId)
        {
            var categories = await _menuRepository.DbSet
                .Where(menu => menu.Id == menuId)
                .AsSingleQuery()
                .Select(menu => new CategoriesOfMenuDto
                {
                    Id = menu.Id,
                    Name = menu.Name,
                    TotalCategory = menu.Categories.Count,
                    CategoryDetails = menu.Categories.Select(category => new CatgoryDetailAndPostDto
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Description = category.Description,
                        MenuId = category.MenuId,
                        TotalPost = category.Posts.Count,
                        Posts = category.Posts.Select(post => new PostDto
                        {
                            Id = post.Id,
                            Title = post.Title,
                            ShortDescription = post.ShortDescription,
                            ThumbnailImageUrl = post.ThumbnailImageUrl,
                            Content = post.Content,
                            CategoryId = post.CategoryId,
                            CreatedBy = post.CreatedBy,
                            UpdatedBy = post.UpdatedBy
                        })
                    })
                })
                .FirstOrDefaultAsync();
            return categories;
        }
    }
}
