﻿using BlogApp.Api.Domain.Common.Constants;
using BlogApp.Api.Domain.Common.Enum;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Infrastructure.DataAccess;
using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.Services
{
    public class CarouselService : ICarouselService
    {
        private readonly IRepository<CarouselEntity, int> _carouselRepository;
        private readonly IUnitOfWork _uow;

        public CarouselService(
            IRepository<CarouselEntity, int> carouselRepository,
            IUnitOfWork uow
        )
        {
            _carouselRepository = carouselRepository;
            _uow = uow;
        }

        public async Task<List<CarouselDetailDto>> GetAll()
        {
            var result = await _carouselRepository.DbSet
                .Select(carousel => new CarouselDetailDto
                {
                    Id = carousel.Id,
                    ImageUrl = carousel.ImageUrl,
                    Link = carousel.Link,
                    Title = carousel.Title,
                    Caption = carousel.Caption,
                    CreatedDate = carousel.CreatedDate
                })
                .ToListAsync();

            return result;
        }

        public async Task<CarouselDetailDto> GetById(int id)
        {
            var result = await _carouselRepository.DbSet
                .Where(menu => menu.Id == id)
                .Select(carousel => new CarouselDetailDto
                {
                    Id = carousel.Id,
                    ImageUrl = carousel.ImageUrl,
                    Link = carousel.Link,
                    Title = carousel.Title,
                    Caption = carousel.Caption,
                    CreatedDate = carousel.CreatedDate
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(CarouselDto carousel)
        {
            var newCarousel = new CarouselEntity
            {
                ImageUrl = carousel.ImageUrl,
                Link = carousel.Link,
                Title = carousel.Title,
                Caption = carousel.Caption
            };

            _carouselRepository.Add(newCarousel);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CreateCarouselSuccessfully,
                Data = newCarousel.Id
            };
        }

        public async Task<ResultDto> Edit(CarouselDto carousel)
        {
            var dbCarousel = await _carouselRepository.FindOneAsync(x => x.Id == carousel.Id);
            if (dbCarousel != null)
            {
                dbCarousel.ImageUrl = carousel.ImageUrl;
                dbCarousel.Link = carousel.Link;
                dbCarousel.Title = carousel.Title;
                dbCarousel.Caption = carousel.Caption;

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.EditCarouselSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.CarouselNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var dbCarousel = await _carouselRepository.FindOneAsync(x => x.Id == id);
            if (dbCarousel != null)
            {
                _carouselRepository.Delete(dbCarousel);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteCarouselSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.CarouselNotFound
            };
        }
    }
}
