﻿using BlogApp.Api.Domain.Common.Constants;
using BlogApp.Api.Domain.Common.Enum;
using BlogApp.Api.Domain.Dto;
using BlogApp.Api.Domain.ServiceContracts;
using BlogApp.Api.Infrastructure.DataAccess;
using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.Services
{
    public class PostService : IPostService
    {
        private readonly IRepository<CategoryEntity, int> _categoryRepository;
        private readonly IRepository<PostEntity, int> _postRepository;
        private readonly IUnitOfWork _uow;
        private readonly Dictionary<string, Func<IQueryable<PostDetailDto>, bool, IOrderedQueryable<PostDetailDto>>> _orderDictionary;

        public PostService(
            IRepository<CategoryEntity, int> categoryRepository,
            IRepository<PostEntity, int> postRepository,
            IUnitOfWork uow
        )
        {
            _categoryRepository = categoryRepository;
            _postRepository = postRepository;
            _uow = uow;

            _orderDictionary = new Dictionary<string, Func<IQueryable<PostDetailDto>, bool, IOrderedQueryable<PostDetailDto>>>
            {
                { nameof(PostDetailDto.Title).ToLower(), OrderByTitle },
                { nameof(PostDetailDto.ShortDescription).ToLower(), OrderByShortDescription },
                { nameof(PostDetailDto.TotalView).ToLower(), OrderByTotalView },
                { nameof(PostDetailDto.CategoryName).ToLower(), OrderByCategoryName },
                { nameof(PostDetailDto.CreatedDate).ToLower(), OrderByCreatedDate }
            };
        }

        public async Task<PaginatedObjectDto<PostDetailDto>> GetPosts(
            string searchValue,
            string sortBy,
            bool isDescending,
            int page = 1,
            int itemsPerPage = 10
        )
        {
            var query = _postRepository.DbSet
                .Select(post => new PostDetailDto
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    Content = post.Content,
                    TotalView = post.TotalView,
                    CategoryId = post.CategoryId,
                    CategoryName = post.Category.Name,
                    CreatedDate = post.CreatedDate,
                    CreatedBy = post.CreatedBy
                });

            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                query = query.Where(post => post.Title.Contains(searchValue));
            }

            if (!string.IsNullOrWhiteSpace(sortBy))
            {
                var orderByFunc = _orderDictionary[sortBy.ToLower()];

                query = orderByFunc(query, isDescending);
            }
            else
            {
                query = query.OrderBy(post => post.Id);
            }

            var total = await query.CountAsync();
            var result = await query
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();

            return new PaginatedObjectDto<PostDetailDto>
            {
                Data = result,
                Total = total,
                Page = page,
                ItemsPerPage = itemsPerPage
            };
        }

        public async Task<List<PostDetailDto>> GetAll()
        {
            var result = await _postRepository.DbSet
                .Select(post => new PostDetailDto
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    Content = post.Content,
                    TotalView = post.TotalView,
                    CategoryId = post.CategoryId,
                    CategoryName = post.Category.Name,
                    CreatedDate = post.CreatedDate
                })
                .ToListAsync();

            return result;
        }

        public async Task<PostDetailDto> GetById(int id)
        {
            var result = await _postRepository.DbSet
                .Where(post => post.Id == id)
                .Select(post => new PostDetailDto
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    Content = post.Content,
                    TotalView = post.TotalView,
                    CategoryId = post.CategoryId,
                    CategoryName = post.Category.Name,
                    CreatedDate = post.CreatedDate,
                    CreatedBy = post.CreatedBy
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<List<PostDetailDto>> GetPopularPost(int numberOfPost)
        {
            var result = await _postRepository.DbSet
                .OrderBy(u => u.TotalView).Take(numberOfPost)
                .Select(post => new PostDetailDto
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    Content = post.Content,
                    TotalView = post.TotalView,
                    CategoryId = post.CategoryId,
                    CategoryName = post.Category.Name,
                    CreatedDate = post.CreatedDate
                }).ToListAsync();

            return result;
        }

        public async Task<ResultDto> Create(PostDto post)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == post.CategoryId);
            if (category != null)
            {
                var newPost = new PostEntity
                {
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    CategoryId = post.CategoryId,
                    Content = post.Content,
                    CreatedBy = post.CreatedBy
                };

                _postRepository.Add(newPost);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.CreatePostSuccessfully,
                    Data = newPost.Id
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.CategoryNotFound
            };
        }

        public async Task<ResultDto> Edit(PostDto post)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == post.CategoryId);
            if (category != null)
            {
                var dbPost = await _postRepository.FindOneAsync(x => x.Id == post.Id);
                if (dbPost != null)
                {
                    dbPost.Title = post.Title;
                    dbPost.ShortDescription = post.ShortDescription;
                    dbPost.ThumbnailImageUrl = post.ThumbnailImageUrl;
                    dbPost.CategoryId = post.CategoryId;
                    dbPost.Content = post.Content;
                    dbPost.UpdatedBy = post.UpdatedBy;

                    await _uow.CommitChangesAsync();

                    return new ResultDto
                    {
                        Status = ActionStatus.Success,
                        Message = Message.EditPostSuccessfully
                    };
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.PostNotFound
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.CategoryNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var post = await _postRepository.FindOneAsync(x => x.Id == id);
            if (post != null)
            {
                _postRepository.Delete(post);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeletePostSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.PostNotFound
            };
        }


        public async Task<List<PostDetailDto>> GetPostsByCategoryId(int categoryId, int postId)
        {
            var query = _postRepository.DbSet
                .Select(post => new PostDetailDto
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImageUrl = post.ThumbnailImageUrl,
                    Content = post.Content,
                    TotalView = post.TotalView,
                    CategoryId = post.CategoryId,
                    CategoryName = post.Category.Name,
                    CreatedDate = post.CreatedDate,
                    CreatedBy = post.CreatedBy
                });

            var result = await query.Where(p => p.CategoryId == categoryId && p.Id != postId).OrderByDescending(s=>s.CreatedDate).Take(3).ToListAsync();

            return result;
        }


        private IOrderedQueryable<PostDetailDto> OrderByTitle(IQueryable<PostDetailDto> query, bool isDescending)
        {
            if (isDescending)
            {
                return query.OrderByDescending(post => post.Title);
            }

            return query.OrderBy(post => post.Title);
        }

        private IOrderedQueryable<PostDetailDto> OrderByShortDescription(IQueryable<PostDetailDto> query, bool isDescending)
        {
            if (isDescending)
            {
                return query.OrderByDescending(post => post.ShortDescription);
            }

            return query.OrderBy(post => post.ShortDescription);
        }

        private IOrderedQueryable<PostDetailDto> OrderByTotalView(IQueryable<PostDetailDto> query, bool isDescending)
        {
            if (isDescending)
            {
                return query.OrderByDescending(post => post.TotalView);
            }

            return query.OrderBy(post => post.TotalView);
        }

        private IOrderedQueryable<PostDetailDto> OrderByCategoryName(IQueryable<PostDetailDto> query, bool isDescending)
        {
            if (isDescending)
            {
                return query.OrderByDescending(post => post.CategoryName);
            }

            return query.OrderBy(post => post.CategoryName);
        }

        private IOrderedQueryable<PostDetailDto> OrderByCreatedDate(IQueryable<PostDetailDto> query, bool isDescending)
        {
            if (isDescending)
            {
                return query.OrderByDescending(post => post.CreatedDate);
            }

            return query.OrderBy(post => post.CreatedDate);
        }
    }
}
