﻿using BlogApp.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.ServiceContracts
{
    public interface ICommentService : IBaseService
    {
        Task<ResultDto> Add(CommentDto comment);
        Task<List<CommentDto>> GetByPostId(int id);
    }
}
