﻿using BlogApp.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.ServiceContracts
{
    public interface IMenuService : IBaseService
    {
        Task<List<MenuDetailDto>> GetAll();
        Task<MenuDetailDto> GetById(int id);
        Task<ResultDto> Create(MenuDto menu);
        Task<ResultDto> Edit(MenuDto menu);
        Task<ResultDto> Delete(int id);
        Task<CategoriesOfMenuDto> GetCategoryAndPostByMenuId(int menuId);
    }
}
