﻿using BlogApp.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.ServiceContracts
{
    public interface ICategoryService : IBaseService
    {
        Task<List<CategoryDetailDto>> GetAll();
        Task<CategoryDetailDto> GetById(int id);
        Task<ResultDto> Create(CategoryDto category);
        Task<ResultDto> Edit(CategoryDto category);
        Task<ResultDto> Delete(int id);
    }
}
