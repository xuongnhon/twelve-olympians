﻿using BlogApp.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.ServiceContracts
{
    public interface ICarouselService : IBaseService
    {
        Task<List<CarouselDetailDto>> GetAll();
        Task<CarouselDetailDto> GetById(int id);
        Task<ResultDto> Create(CarouselDto carousel);
        Task<ResultDto> Edit(CarouselDto carousel);
        Task<ResultDto> Delete(int id);
    }
}
