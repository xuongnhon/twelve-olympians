﻿using BlogApp.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Api.Domain.ServiceContracts
{
    public interface IPostService : IBaseService
    {
        Task<PaginatedObjectDto<PostDetailDto>> GetPosts(
            string searchValue,
            string sortBy,
            bool isDescending,
            int page = 1,
            int itemsPerPage = 10
        );
        Task<List<PostDetailDto>> GetAll();
        Task<PostDetailDto> GetById(int id);
        Task<ResultDto> Create(PostDto post);
        Task<ResultDto> Edit(PostDto post);
        Task<ResultDto> Delete(int id);
        Task<List<PostDetailDto>> GetPostsByCategoryId(int categoryId, int postId);
        Task<List<PostDetailDto>> GetPopularPost(int numberOfPost);
    }
}
