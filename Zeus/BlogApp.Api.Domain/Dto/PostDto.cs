﻿namespace BlogApp.Api.Domain.Dto
{
    public class PostDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
