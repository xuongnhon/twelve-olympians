﻿using System;

namespace BlogApp.Api.Domain.Dto
{
    public class CommentDto : BaseDto<int>
    {
        public int PostId { get; set; }
        public long? CommentId { get; set; }
        public string Content { get; set; }
        public string CommentBy { get; set; }
        public DateTime? CommentDate { get; set; }
    }
}
