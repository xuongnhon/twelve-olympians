﻿using BlogApp.Api.Domain.Common.Enum;

namespace BlogApp.Api.Domain.Dto
{
    public class ResultDto
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
