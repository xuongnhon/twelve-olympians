﻿using System;

namespace BlogApp.Api.Domain.Dto
{
    public class MenuDetailDto : BaseDto<int>
    {
        public string Name { get; set; }
        public int NumberOfCategories { get; set; }
        public int NumberOfPosts { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
