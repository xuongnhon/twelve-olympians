﻿namespace BlogApp.Api.Domain.Dto
{
    public class MenuDto : BaseDto<int>
    {
        public string Name { get; set; }
    }
}
