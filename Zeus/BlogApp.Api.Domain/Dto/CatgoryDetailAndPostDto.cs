using System.Collections.Generic;

namespace BlogApp.Api.Domain.Dto
{
    public class CatgoryDetailAndPostDto : BaseDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
        public int TotalPost { get; set; }
        public IEnumerable<PostDto> Posts { get; set; }
    }
}