using System.Collections.Generic;

namespace BlogApp.Api.Domain.Dto
{
    public class CategoriesOfMenuDto : BaseDto<int>
    {
        public string Name { get; set; }
        public int TotalCategory { get; set; }
        public IEnumerable<CatgoryDetailAndPostDto> CategoryDetails { get; set; }
    }
}