﻿namespace BlogApp.Api.Domain.Dto
{
    public class CarouselDto : BaseDto<int>
    {
        public string ImageUrl { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
    }
}
