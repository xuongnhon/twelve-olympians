﻿using System;

namespace BlogApp.Api.Domain.Dto
{
    public class PostDetailDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string Content { get; set; }
        public int TotalView { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
