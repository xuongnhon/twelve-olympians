﻿namespace BlogApp.Api.Domain.Dto
{
    public abstract class BaseDto<T>
    {
        public T Id { get; set; }
    }
}
