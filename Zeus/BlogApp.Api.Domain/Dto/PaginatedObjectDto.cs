﻿using System.Collections.Generic;

namespace BlogApp.Api.Domain.Dto
{
    public class PaginatedObjectDto<T>
    {
        public List<T> Data { get; set; }
        public int Total { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
