﻿using System;

namespace BlogApp.Api.Domain.Dto
{
    public class CategoryDetailDto : BaseDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int NumberOfPosts { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
