﻿namespace BlogApp.Api.Domain.Dto
{
    public class CategoryDto : BaseDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
    }
}
