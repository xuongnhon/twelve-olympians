﻿using System;

namespace BlogApp.Api.Domain.Dto
{
    public class CarouselDetailDto : BaseDto<int>
    {
        public string ImageUrl { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
