﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Api.Infrastructure.Entities
{
    public class PostEntity : BaseEntity<int>
    {
        [MaxLength(150)]
        public string Title { get; set; }
        [MaxLength(1500)]
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public int TotalView { get; set; }

        public int CategoryId { get; set; }
        public CategoryEntity Category { get; set; }

        public IList<CommentEntity> PostComments { get; set; }
    }
}
