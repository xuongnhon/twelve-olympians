﻿using System.Collections.Generic;

namespace BlogApp.Api.Infrastructure.Entities
{
    public class CommentEntity : BaseEntity<long>
    {
        public string Content { get; set; }

        public int PostId { get; set; }
        public PostEntity Post { get; set; }

        public long? CommentId { get; set; }
        public CommentEntity Comment { get; set; }
        public IList<CommentEntity> Comments { get; set; }
    }
}
