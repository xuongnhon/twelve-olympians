﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Api.Infrastructure.Entities
{
    public class CategoryEntity : BaseEntity<int>
    {
        [MaxLength(150)]
        public string Name { get; set; }
        public string Description { get; set; }

        public int MenuId { get; set; }
        public MenuEntity Menu { get; set; }

        public IList<PostEntity> Posts { get; set; }
    }
}
