﻿namespace BlogApp.Api.Infrastructure.Entities
{
    public class CarouselEntity : BaseEntity<int>
    {
        public string ImageUrl { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
    }
}
