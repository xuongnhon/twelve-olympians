﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Api.Infrastructure.Entities
{
    public abstract class BaseEntity<T>
    {
        [Key]
        public T Id { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
    }
}
