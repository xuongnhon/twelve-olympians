﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Api.Infrastructure.Entities
{
    public class MenuEntity : BaseEntity<int>
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public IList<CategoryEntity> Categories { get; set; }
    }
}
