﻿using BlogApp.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.Context
{
    public class BlogAppDbContext : DbContext
    {
        public BlogAppDbContext(DbContextOptions<BlogAppDbContext> options)
            : base(options)
        {

        }

        public DbSet<MenuEntity> Menus { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<PostEntity> Posts { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<CarouselEntity> Carousels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MenuEntity>(m =>
            {
                m.HasMany(x => x.Categories).WithOne(x => x.Menu).HasForeignKey(x => x.MenuId).IsRequired();
            });

            modelBuilder.Entity<CategoryEntity>(m =>
            {
                m.HasOne(x => x.Menu).WithMany(x => x.Categories).HasForeignKey(x => x.MenuId).IsRequired();
                m.HasMany(x => x.Posts).WithOne(x => x.Category).HasForeignKey(x => x.CategoryId).IsRequired();
            });

            modelBuilder.Entity<PostEntity>(m =>
            {
                m.HasOne(x => x.Category).WithMany(x => x.Posts).HasForeignKey(x => x.CategoryId).IsRequired();
                m.HasMany(x => x.PostComments).WithOne(x => x.Post).HasForeignKey(x => x.PostId).IsRequired();
            });

            modelBuilder.Entity<CommentEntity>(m =>
            {
                m.HasOne(x => x.Post).WithMany(x => x.PostComments).HasForeignKey(x => x.PostId).IsRequired();
                m.HasOne(x => x.Comment).WithMany(x => x.Comments).HasForeignKey(x => x.CommentId).IsRequired(false);
            });


            modelBuilder.Entity<CarouselEntity>(m =>
            {

            });

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            AddAuditData();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddAuditData();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AddAuditData();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            AddAuditData();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected void AddAuditData()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity<int> && (x.State == EntityState.Added || x.State == EntityState.Modified))
                .ToList();

            if (entities.Count > 0)
            {
                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {
                        ((BaseEntity<int>)entity.Entity).CreatedDate = DateTime.UtcNow;
                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        ((BaseEntity<int>)entity.Entity).UpdatedDate = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}
