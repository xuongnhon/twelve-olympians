﻿using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.DataAccess
{
    public interface IUnitOfWork
    {
        void CommitChanges();
        Task CommitChangesAsync();
    }
}
