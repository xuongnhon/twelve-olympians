﻿using BlogApp.Api.Infrastructure.Context;
using System;
using System.Threading.Tasks;

namespace BlogApp.Api.Infrastructure.DataAccess
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed;
        private readonly BlogAppDbContext _blogAppDbContext;

        public UnitOfWork(BlogAppDbContext blogAppDbContext)
        {
            _blogAppDbContext = blogAppDbContext;
        }

        public virtual void CommitChanges()
        {
            _blogAppDbContext.SaveChanges();
        }

        public virtual async Task CommitChangesAsync()
        {
            await _blogAppDbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _blogAppDbContext.Dispose();
            }

            _disposed = true;
        }
    }
}
