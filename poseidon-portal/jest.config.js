module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  setupFilesAfterEnv: ['./jest.setup.js'],
  testResultsProcessor: "jest-teamcity-reporter"
}
