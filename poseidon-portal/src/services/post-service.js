import Vue from "vue";

const CarouselService = {
  getPosts(request) {
    return Vue.$http.get("posts", {
      params: {
        searchValue: request.searchValue,
        sortBy: request.sortBy,
        isDescending: request.isDescending,
        page: request.page,
        itemsPerPage: request.itemsPerPage,
      },
    });
  },
  getById(id) {
    return Vue.$http.get("posts/" + id);
  },
  add(formData) {
    return Vue.$http.post("posts/add", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  edit(formData) {
    return Vue.$http.put("posts/edit", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  delete(id) {
    return Vue.$http.delete("posts/delete/" + id);
  },
};

export default CarouselService;
