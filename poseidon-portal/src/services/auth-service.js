import Oidc from "oidc-client";

const config = {
  authority: process.env.VUE_APP_AUTHORITY_PATH,
  client_id: process.env.VUE_APP_CLIENT_ID,

  redirect_uri: `${window.location.origin}${process.env.VUE_APP_PATHBASE}Oauth/Callback`,
  post_logout_redirect_uri: `${window.location.origin}${process.env.VUE_APP_PATHBASE}Oauth/SignoutCallback`,
  silent_redirect_uri: `${window.location.origin}${process.env.VUE_APP_PATHBASE}Oauth/Silent`,

  response_type: "code",
  userStore: new Oidc.WebStorageStateStore(),

  scope: "openid profile BlogApp.Api",
  filterProtocolClaims: true,
  loadUserInfo: true,

  automaticSilentRenew: true,
  revokeAccessTokenOnSignout: true,
};

const oidcManager = new Oidc.UserManager(config);

const AuthService = {
  async getUser() {
    return await oidcManager.getUser();
  },
  async getAccessToken() {
    let user = await this.getUser();

    if (user) {
      return user.access_token;
    }

    return null;
  },
  async isAuthenticated() {
    let user = await this.getUser();

    if (user) {
      return !user.expired;
    }

    return false;
  },
  login() {
    return oidcManager.signinRedirect();
  },
  logout() {
    return oidcManager.signoutRedirect();
  },
  async signinRedirectCallback() {
    return await oidcManager.signinRedirectCallback();
  },
  async signinSilentCallback() {
    return await oidcManager.signinSilentCallback();
  },
  async signoutRedirectCallback() {
    return await oidcManager.signoutRedirectCallback();
  },
};

export default AuthService;
