import Vue from "vue";

const UserService = {
  getAll() {
    return Vue.$http.get("users");
  },
  getById(id) {
    return Vue.$http.get("users/" + id);
  },
  checkUserName(userName) {
    return Vue.$http.get("users/check/" + userName);
  },
  add(user) {
    return Vue.$http.post("users/add", user);
  },
  edit(user) {
    return Vue.$http.put("users/edit", user);
  },
  delete(id) {
    return Vue.$http.delete("users/delete/" + id);
  },
};

export default UserService;
