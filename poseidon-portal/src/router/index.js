import Vue from "vue";
import VueRouter from "vue-router";
import MainLayout from "@/components/layouts/MainLayout.vue";
import ExternalLayout from "@/components/layouts/ExternalLayout.vue";
import AuthRoutes from "./auth-routes";
import AuthService from "@/services/auth-service";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: MainLayout,
    children: [
      {
        path: "",
        name: "Home",
        component: () => import("../views/Home.vue"),
        meta: {
          title: "Home Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Menus",
        name: "Menus",
        component: () => import("../views/menu/Index.vue"),
        meta: {
          title: "Menu Management",
          requiresAuth: true,
        },
      },
      {
        path: "/Menus/Create",
        name: "CreateMenu",
        component: () => import("../views/menu/CreateMenu.vue"),
        meta: {
          title: "Create Menu Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Menus/Edit/:id",
        name: "EditMenu",
        component: () => import("../views/menu/EditMenu.vue"),
        meta: {
          title: "Edit Menu Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Carousels",
        name: "Carousels",
        component: () => import("../views/carousel/Index.vue"),
        meta: {
          title: "Carousel Management",
          requiresAuth: true,
        },
      },
      {
        path: "/Carousels/Create",
        name: "CreateCarousel",
        component: () => import("../views/carousel/CreateCarousel.vue"),
        meta: {
          title: "Create Carousel Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Carousels/Edit/:id",
        name: "EditCarousel",
        component: () => import("../views/carousel/EditCarousel.vue"),
        meta: {
          title: "Edit Carousel Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Categories",
        name: "Categories",
        component: () => import("../views/category/Index.vue"),
        meta: {
          title: "Category Management",
          requiresAuth: true,
        },
      },
      {
        path: "/Categories/Create",
        name: "CreateCategory",
        component: () => import("../views/category/CreateCategory.vue"),
        meta: {
          title: "Create Category Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Categories/Edit/:id",
        name: "EditCategory",
        component: () => import("../views/category/EditCategory.vue"),
        meta: {
          title: "Edit Category Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Posts",
        name: "Posts",
        component: () => import("../views/post/Index.vue"),
        meta: {
          title: "Post Management",
          requiresAuth: true,
        },
      },
      {
        path: "/Posts/Create",
        name: "CreatePost",
        component: () => import("../views/post/CreatePost.vue"),
        meta: {
          title: "Create Post Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Posts/Edit/:id",
        name: "EditPost",
        component: () => import("../views/post/EditPost.vue"),
        meta: {
          title: "Edit Post Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Users",
        name: "Users",
        component: () => import("../views/user/Index.vue"),
        meta: {
          title: "User Management",
          requiresAuth: true,
        },
      },
      {
        path: "/Users/Create",
        name: "CreateUser",
        component: () => import("../views/user/CreateUser.vue"),
        meta: {
          title: "Create User Page",
          requiresAuth: true,
        },
      },
      {
        path: "/Users/Edit/:id",
        name: "EditUser",
        component: () => import("../views/user/EditUser.vue"),
        meta: {
          title: "Edit User Page",
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: "/PageNotFound",
    component: ExternalLayout,
    children: [
      {
        path: "/PageNotFound",
        name: "PageNotFound",
        component: () => import("../views/PageNotFound.vue"),
        meta: {
          title: "Page Not Found",
          requiresAuth: false,
        },
      },
    ],
  },
  { path: "*", redirect: "/PageNotFound" },
  ...AuthRoutes,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.title);

  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  } else if (previousNearestWithMeta) {
    document.title = previousNearestWithMeta.meta.title;
  }

  if (!to.matched.some((record) => record.meta.requiresAuth)) {
    next();
    return;
  }

  let isAuthenticated = await AuthService.isAuthenticated();
  if (!isAuthenticated) {
    AuthService.login();
    return;
  }

  next();
});

export default router;
