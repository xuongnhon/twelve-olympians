import ExternalLayout from "@/components/layouts/ExternalLayout.vue";

const routes = [
  {
    path: "/Oauth",
    component: ExternalLayout,
    children: [
      {
        path: "/Oauth/AccessDenied",
        name: "AccessDenied",
        component: () => import("../views/auth/AccessDenied.vue"),
        meta: {
          title: "Access Denied",
          requiresAuth: false,
        },
      },
      {
        path: "/Oauth/Callback",
        name: "SigninCallback",
        component: () => import("../views/auth/SigninCallback.vue"),
        meta: {
          title: "Waiting...",
          requiresAuth: false,
        },
      },
      {
        path: "/Oauth/LogOut",
        name: "LogOut",
        component: () => import("../views/auth/LogOut.vue"),
        meta: {
          title: "Logout",
          requiresAuth: false,
        },
      },
      {
        path: "/Oauth/SignoutCallback",
        name: "SignoutCallback",
        component: () => import("../views/auth/SignoutCallback.vue"),
        meta: {
          title: "Waiting...",
          requiresAuth: false,
        },
      },
      {
        path: "/Oauth/Silent",
        name: "Silent",
        component: () => import("../views/auth/Silent.vue"),
        meta: {
          title: "Slient",
          requiresAuth: false,
        },
      },
    ],
  },
];

export default routes;
