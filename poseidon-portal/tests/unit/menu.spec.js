import Vuetify from "vuetify"
import Vuex from "vuex"
import { createLocalVue, mount } from "@vue/test-utils";

import Menu from "@/views/menu/Index.vue";
import MenuService from "@/services/menu-service";
import router from "@/router"

describe("Menu page", () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  document.body.setAttribute('data-app', true)
  let store;
  let vuetify;

  let menuServiceGetAllMock;
  let menuServiceDeleteMock;
  let routerPushMock;

  const setMenuServiceReturnEmpty = () => {
    menuServiceGetAllMock.mockImplementation(() => Promise.resolve({
      data: {
        isSuccess: true,
        data: []
      }
    }));
  };

  const menu = {
    id: 1,
    name: ".Net",
    numberOfCategories: 0,
    numberOfPosts: 0,
    createdDate: "2021-07-31T04:23:34.1504677"
  };

  const setMenuServiceReturnData = () => {
    menuServiceGetAllMock.mockImplementation(() => Promise.resolve({
      data: {
        isSuccess: true,
        data: [menu]
      }
    }));
  };

  beforeEach(() => {
    vuetify = new Vuetify();

    store = new Vuex.Store({
      namespaced: true,
      actions: {
        "application/showLoading": jest.fn(),
        "application/hideLoading": jest.fn()
      }
    });

    menuServiceGetAllMock = jest.spyOn(MenuService, "getAll");
    menuServiceDeleteMock = jest.spyOn(MenuService, "delete");
  });

  afterEach(() => {
    if (menuServiceGetAllMock) {
      menuServiceGetAllMock.mockClear();
    }

    if (menuServiceDeleteMock) {
      menuServiceDeleteMock.mockClear();
    }

    if (routerPushMock) {
      routerPushMock.mockClear();
    }
  });

  it("Should have a title", () => {
    setMenuServiceReturnEmpty();
    const title = "Menu management";

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });

    expect(wrapper.find("div.row > div.col.md-12 > h2").text()).toEqual(title);
  });

  it("Should have a create button", () => {
    setMenuServiceReturnEmpty();
    const buttonText = "Create";

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });

    expect(wrapper.find("div.row > div.col.md-12 > button.v-btn span.v-btn__content").text()).toEqual(buttonText);
  });

  it("Should have a table", () => {
    setMenuServiceReturnEmpty();

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });

    expect(wrapper.find("div.row > div.col.md-12 > div.v-data-table")).not.toBeNull();
  });

  it("Should redirect to create menu page when user clicks on the create button", async () => {
    setMenuServiceReturnEmpty();

    const wrapper = mount(Menu, {
      localVue,
      vuetify,
      router
    });
    routerPushMock = jest.spyOn(wrapper.vm.$router, "push");
    const createButton = wrapper.find("div.row > div.col.md-12 > button.v-btn");
    await createButton.trigger("click");

    expect(routerPushMock.mock.calls.length).toBe(1);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "CreateMenu" });
  });

  it("Should show a message when don't have any menus", async () => {
    setMenuServiceReturnEmpty();
    const message = "No data available";

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();

    expect(wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td").text()).toEqual(message);
  });

  it("Table should have 4 columns", async () => {
    setMenuServiceReturnEmpty();

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();
    const columns = wrapper.findAll("div.row > div.col.md-12 > div.v-data-table table thead tr th");

    expect(columns.length).toEqual(4);
  });

  it("Table should show correct data and action buttons", async () => {
    setMenuServiceReturnData();

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();
    const columns = wrapper.findAll("div.row > div.col.md-12 > div.v-data-table table tbody tr td");

    expect(columns.length).toEqual(4);
    expect(columns.at(0).text()).toEqual(menu.name);
    expect(columns.at(1).text()).toEqual(menu.numberOfCategories.toString());
    expect(columns.at(2).text()).toEqual(menu.createdDate);
    expect(columns.at(3).findAll("button").length).toEqual(2);
  });

  it("Should redirect to edit menu page when user clicks on the edit button", async () => {
    setMenuServiceReturnData();

    const wrapper = mount(Menu, {
      localVue,
      vuetify,
      router
    });
    await wrapper.vm.$nextTick();
    routerPushMock = jest.spyOn(wrapper.vm.$router, "push");
    const button = wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td button.v-btn.cyan--text");
    await button.trigger("click");

    expect(routerPushMock.mock.calls.length).toBe(1);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "EditMenu", params: { id: menu.id } });
  });

  it("Should show confirm dialog when user clicks on the delete button", async () => {
    setMenuServiceReturnData();

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();
    const button = wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td button.v-btn.error--text");
    await button.trigger("click");
    const dialog = wrapper.find(".v-dialog.v-dialog--active");

    expect(dialog).not.toBeNull();
  });

  it("Should close confirm dialog when user clicks on the cancel button", async () => {
    setMenuServiceReturnData();

    const wrapper = mount(Menu, {
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();
    const deleteButton = wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td button.v-btn.error--text");
    await deleteButton.trigger("click");
    const cancelButton = wrapper.find(".v-dialog button.v-btn:not(.error--text)");
    await cancelButton.trigger("click");

    const dialog = wrapper.find(".v-dialog.v-dialog--active");

    expect(dialog.exists()).toBe(false);
  });

  it("Should delete a menu successfully", async () => {
    setMenuServiceReturnData();
    const message = "No data available";
    menuServiceDeleteMock.mockImplementation(() => {
      setMenuServiceReturnEmpty();

      return Promise.resolve({
        data: {
          isSuccess: true
        }
      })
    });

    const wrapper = mount(Menu, {
      store,
      localVue,
      vuetify
    });
    await wrapper.vm.$nextTick();
    const deleteButton = wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td button.v-btn.error--text");
    await deleteButton.trigger("click");

    const confirmButton = wrapper.find(".v-dialog button.v-btn.error--text");
    await confirmButton.trigger("click");
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();
    expect(wrapper.find("div.row > div.col.md-12 > div.v-data-table table tbody tr td").text()).toEqual(message);
  });
})
