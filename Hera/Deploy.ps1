Write-Host "Octopus parameters:"
Write-Host "	HeraContainerName=$HeraContainerName"
Write-Host "	HeraVolume=$HeraVolume"
Write-Host "	HeraHttpPort=$HeraHttpPort"
Write-Host "	HeraHttpsPort=$HeraHttpsPort"
Write-Host "	HeraHttp2Port=$HeraHttp2Port"
Write-Host "	PoseidonHost=$PoseidonHost"
Write-Host "	PoseidonPort=$PoseidonPort"
Write-Host "	DionysusHost=$DionysusHost"
Write-Host "	DionysusPort=$DionysusPort"

$container = (docker ps -qa -f name=$HeraContainerName)
if ($container -ne $null) {
	Write-Host "Removing container..."
	docker rm $HeraContainerName -f
	Write-Host "Done!"
} else {
	Write-Host "Container isn't running."
}

$currentFolder = Get-Location
$filePath = Get-ChildItem -Path $currentFolder -Filter *.tar | Select -First 1
$fileName = [io.path]::GetFileNameWithoutExtension($filePath)
$version = $fileName.replace("hera.", '')

Write-Host "Loading image..."
docker load -i $filePath
Write-Host "Done!"

Write-Host "Starting..."
$command = "docker run -d -v $HeraVolume" + ":/app --name $HeraContainerName -p $HeraHttpPort" + ":6062 -p $HeraHttpsPort" + ":6063 -p $HeraHttp2Port" + ":7071 -e Clients__0__Name=PoseidonClient -e Clients__0__Host=$PoseidonHost -e Clients__0__Port=$PoseidonPort -e Clients__1__Name=DionysusClient -e Clients__1__Host=$DionysusHost -e Clients__1__Port=$DionysusPort -e ASPNETCORE_Kestrel__Certificates__Default__Password=P@ssw0rd -e ASPNETCORE_Kestrel__Certificates__Default__Path=aspnetapp.pfx hera:$version"
iex "& $command"
Write-Host "Done!"
