﻿using BlogApp.Idp.Config;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace BlogApp.Idp
{
    public static class IdentityConfig
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource {
                    Name = "BlogApp.Api"
                }
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>
            {
                new ApiResource {
                    Name = "BlogApp.Api",
                    DisplayName = "BlogApp.Api",
                    UserClaims = { "role" },
                    ApiSecrets = { new Secret("e579879b-ad1f-4e6f-a73e-0eb452b6819c".Sha256()) },
                    Scopes = {
                        "BlogApp.Api"
                    }
               }
            };

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            var clients = configuration.GetSection("Clients").Get<ClientConfig[]>();

            return clients.Select(client => new Client
            {
                ClientId = client.Name,
                RequireClientSecret = false,

                AllowedGrantTypes = GrantTypes.Code,
                AccessTokenType = AccessTokenType.Reference,

                RedirectUris = {
                        $"http://{client.Host}:{client.Port}/Oauth/Callback",
                        $"http://{client.Host}:{client.Port}/Oauth/Silent"
                    },
                PostLogoutRedirectUris = { $"http://{client.Host}:{client.Port}/Oauth/SignoutCallback" },

                AllowOfflineAccess = true,
                AllowedScopes = { "openid", "profile", "BlogApp.Api" },
            }).ToArray();
        }
    }
}
