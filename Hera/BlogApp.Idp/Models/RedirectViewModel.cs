namespace BlogApp.Idp.Models
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}
