﻿namespace BlogApp.Idp.Config
{
    public class ClientConfig
    {
        public string Name { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }
    }
}
