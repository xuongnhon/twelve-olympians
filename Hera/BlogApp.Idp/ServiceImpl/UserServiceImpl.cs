﻿using BlogApp.Idp.Api;
using BlogApp.Idp.Dto;
using BlogApp.Idp.ServiceContracts;
using Grpc.Core;
using System.Linq;
using System.Threading.Tasks;
using static BlogApp.Idp.Api.UserService;

namespace BlogApp.Idp.ServiceImpl
{
    public class UserServiceImpl : UserServiceBase
    {
        private readonly IUserService _userService;

        public UserServiceImpl(
            IUserService userService
        )
        {
            _userService = userService;
        }

        public override async Task<GetUsersResponse> GetUsers(GetUsersRequest request, ServerCallContext context)
        {
            var users = (await _userService.GetAll())
                .Select(user => new User
                {
                    Id = user.Id,
                    Name = user.Name,
                    RoleId = user.RoleId,
                    RoleName = user.RoleName,
                    UserName = user.UserName
                }).ToList();

            return new GetUsersResponse
            {
                StatusCode = Api.StatusCode.Success,
                Data = { users }
            };
        }

        public override async Task<GetUserByIdResponse> GetUserById(GetUserByIdRequest request, ServerCallContext context)
        {
            var user = await _userService.GetById(request.Id);

            return new GetUserByIdResponse
            {
                StatusCode = Api.StatusCode.Success,
                Data = new User
                {
                    Id = user.Id,
                    Name = user.Name,
                    RoleId = user.RoleId,
                    RoleName = user.RoleName,
                    UserName = user.UserName
                }
            };
        }

        public override async Task<CheckUserNameResponse> CheckUserName(CheckUserNameRequest request, ServerCallContext context)
        {
            var result = await _userService.CheckUserName(request.UserName);

            return new CheckUserNameResponse
            {
                StatusCode = Api.StatusCode.Success,
                Data = result
            };
        }

        public override async Task<CreateUserResponse> CreateUser(CreateUserRequest request, ServerCallContext context)
        {
            var user = new UserDto
            {
                Name = request.Name,
                UserName = request.UserName,
                Password = request.Password,
                RoleId = request.RoleId
            };
            var result = await _userService.Create(user);

            if (result.Status == Common.Enum.ActionStatus.Success)
            {
                return new CreateUserResponse
                {
                    StatusCode = Api.StatusCode.Success,
                    Data = (string)result.Data
                };
            }

            return new CreateUserResponse
            {
                StatusCode = Api.StatusCode.Error,
                Message = result.Message
            };
        }

        public override async Task<EditUserResponse> EditUser(EditUserRequest request, ServerCallContext context)
        {
            var user = new UserDto
            {
                Id = request.Id,
                Name = request.Name,
                RoleId = request.RoleId
            };
            var result = await _userService.Edit(user);

            if (result.Status == Common.Enum.ActionStatus.Success)
            {
                return new EditUserResponse
                {
                    StatusCode = Api.StatusCode.Success,
                };
            }

            return new EditUserResponse
            {
                StatusCode = Api.StatusCode.Error,
                Message = result.Message
            };
        }

        public override async Task<DeleteUserResponse> DeleteUser(DeleteUserRequest request, ServerCallContext context)
        {
            var result = await _userService.Delete(request.Id);

            if (result.Status == Common.Enum.ActionStatus.Success)
            {
                return new DeleteUserResponse
                {
                    StatusCode = Api.StatusCode.Success
                };
            }

            return new DeleteUserResponse
            {
                StatusCode = Api.StatusCode.Error,
                Message = result.Message
            };
        }
    }
}
