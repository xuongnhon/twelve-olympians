﻿using BlogApp.Idp.Common.Enum;

namespace BlogApp.Idp.Dto
{
    public class ResultDto
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
