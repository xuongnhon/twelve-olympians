import Vue from "vue";
import VueRouter from "vue-router";
import MainLayout from "@/components/layouts/MainLayout.vue";
import ExternalLayout from "@/components/layouts/ExternalLayout.vue";
import AuthRoutes from "./auth-routes";
import AuthService from "@/services/auth-service";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: MainLayout,
    children: [
      {
        path: "",
        name: "Home",
        component: () => import("../views/Home.vue"),
        meta: {
          title: "Home Page",
          requiresAuth: false,
        },
      },
      {
        path: "/Post/:id",
        name: "Post",
        component: () => import("../views/Post.vue"),
        meta: {
          title: "Post",
          requiresAuth: false,
        },
      },
      {
        path: "/Search",
        name: "Search",
        component: () => import("../views/Search.vue"),        
        meta: {
          title: "Search",
          requiresAuth: false,
        },
      },
      {
        path: "/Menu/:id",
        name: "Menu",
        component: () => import("../views/Menu.vue"),
        meta: {
          title: "Menu",
          requiresAuth: false,
        },
      }
    ],
  },
  {
    path: "/PageNotFound",
    component: ExternalLayout,
    children: [
      {
        path: "/PageNotFound",
        name: "PageNotFound",
        component: () => import("../views/PageNotFound.vue"),
        meta: {
          title: "Page Not Found",
          requiresAuth: false,
        },
      },
    ],
  },
  { path: "*", redirect: "/PageNotFound" },
  ...AuthRoutes,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.title);

  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  } else if (previousNearestWithMeta) {
    document.title = previousNearestWithMeta.meta.title;
  }

  if (!to.matched.some((record) => record.meta.requiresAuth)) {
    next();
    return;
  }

  let isAuthenticated = await AuthService.isAuthenticated();
  if (!isAuthenticated) {
    AuthService.login();
    return;
  }

  next();
});

export default router;
