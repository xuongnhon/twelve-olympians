import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import interceptor from "@/interceptors/axios-interceptor";

import "@/assets/styles/main.scss";
import "vue-loading-overlay/dist/vue-loading.css";

const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});
Vue.$http = interceptor.addTo(axiosInstance);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
