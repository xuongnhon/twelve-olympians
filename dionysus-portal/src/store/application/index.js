import * as mutationTypes from "./mutation-types";
import * as actionTypes from "./action-types";

const state = {
  isLoading: false,
};

const getters = {
  isLoading: (state) => {
    return state.isLoading;
  },
};

const mutations = {
  [mutationTypes.UPDATE_LOADING_STATUS](state, isLoading) {
    state.isLoading = isLoading;
  },
};

const actions = {
  async [actionTypes.SHOW_LOADING](context) {
    context.commit(mutationTypes.UPDATE_LOADING_STATUS, true);
  },
  async [actionTypes.HIDE_LOADING](context) {
    context.commit(mutationTypes.UPDATE_LOADING_STATUS, false);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
