import Vue from "vue";
import Vuex from "vuex";
import applicationModule from "./application";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    application: applicationModule,
  },
});
