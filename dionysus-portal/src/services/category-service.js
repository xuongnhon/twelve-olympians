import Vue from "vue";

const CategoryService = {
  getAll() {
    return Vue.$http.get("categories");
  },
  getById(id) {
    return Vue.$http.get("categories/" + id);
  },
  add(category) {
    return Vue.$http.post("categories/add", category);
  },
  edit(category) {
    return Vue.$http.put("categories/edit", category);
  },
  delete(id) {
    return Vue.$http.delete("categories/delete/" + id);
  },
};

export default CategoryService;
