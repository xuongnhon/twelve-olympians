import Vue from "vue";

const PostService = {
  getPosts(request) {
    return Vue.$http.get("getposts", {
      params: {
        searchValue: request.searchValue,
        sortBy: request.sortBy,
        isDescending: request.isDescending,
        page: request.page,
        itemsPerPage: request.itemsPerPage,
      },
    });
  },
  getById(id) {
    return Vue.$http.get("posts/" + id);
  },
  getPopularPost(request) {
    return Vue.$http.get("posts/popularPosts", {
      params: {
        numberOfPost: request.numberOfPost
      }
    });
  },
  add(formData) {
    return Vue.$http.post("posts/add", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  edit(formData) {
    return Vue.$http.put("posts/edit", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  delete(id) {
    return Vue.$http.delete("posts/delete/" + id);
  },
  getByCategoryId(categoryId, postId){
    return Vue.$http.get("/posts/GetByCategoryId/"+categoryId+"/"+postId);
  }
};

export default PostService;
