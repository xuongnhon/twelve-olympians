import Vue from "vue";

const CommentService = {

  getByPostId(id) {
    return Vue.$http.get("comments/" + id);
  },
  add(formData) {
    return Vue.$http.post("comments/add", formData, {
      headers: {
        "Content-Type": "application/json-patch+json",
      },
    });
  },


};

export default CommentService;
