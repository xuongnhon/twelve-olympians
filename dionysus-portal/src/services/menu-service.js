import Vue from "vue";

const MenuService = {
  getAll() {
    return Vue.$http.get("getmenus");
  },
  getById(id) {
    return Vue.$http.get("menus/" + id);
  },
  add(menu) {
    return Vue.$http.post("menus/add", menu);
  },
  edit(menu) {
    return Vue.$http.put("menus/edit", menu);
  },
  delete(id) {
    return Vue.$http.delete("menus/delete/" + id);
  },
  getCategoriesAndPostsById(id) {
    return Vue.$http.get("menus/categories/" + id)
  }
};

export default MenuService;
