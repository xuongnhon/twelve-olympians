import Vue from "vue";

const CarouselService = {
  getAll() {
    return Vue.$http.get("carousels");
  },
  getByPage(page) {
    return Vue.$http.get("carousels/getByPage?page=" + page);
  },
  getById(id) {
    return Vue.$http.get("carousels/" + id);
  },
  add(formData) {
    return Vue.$http.post("carousels/add", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  edit(formData) {
    return Vue.$http.put("carousels/edit", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  delete(id) {
    return Vue.$http.delete("carousels/delete/" + id);
  },
};

export default CarouselService;
