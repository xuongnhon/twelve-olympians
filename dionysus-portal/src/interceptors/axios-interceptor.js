import AuthService from "@/services/auth-service";

export default {
  addTo(axiosInstance) {
    axiosInstance.interceptors.request.use(
      async (config) => {
        config.headers["Pragma"] = "no-cache";
        let accessToken = await AuthService.getAccessToken();
        if (accessToken) {
          config.headers.common["Authorization"] = `Bearer ${accessToken}`;
        }

        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    axiosInstance.interceptors.response.use(
      (response) => response,
      (error) => {
        return Promise.reject(error);
      }
    );

    return axiosInstance;
  },
};
